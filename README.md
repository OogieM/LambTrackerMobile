LambTracker Mobile
=================

Android program for managing a sheep flock. Uses both EID and visual tagging to document sheep. 

We are shepherds, programming of this item is happening in modules as we need to do different tasks with the flock during the year.

We've been running with LambTracker now since 2012. We use it to track all sheep management tasks, ear tag changes and genetic results.

System is based on an SQLite database for all flock data.

My basic database philosophy is that we don't delete data but only add. 

Some things may change but in general we try to keep a history file so you can go back and see what has been done.
The goal is to have a good data trail of everything that has happened to a sheep over its lifetime.

Currently I set up the database on my desktop system by hand using various SQLite management tools. 

Then the database is moved to the Android and used there.

Reports out are often also done back on the desktop.

My programming season is typically in the fall and winter when there is less sheep flock work to do.
During spring and summer I only work on issues that directly affect things I am doing with the sheep. 

2018-2019 was focused on changing the underlying database structure to make it more normalized and more adaptable.
    We now track location and ownership changes of our sheep although I do not always keep it current as their ownership changes.
    We also learned that menus and choices needed to be in a specific user defined order not just as created. That alone broke nearly 
    all my look-up code and has forced a re-write of much of the day to day flock management work. 
    
2019-2020 development of the mobile version is to clean up the code and continue to handle bugs introduced with changes to the database structure. 

I am also now working on the Desktop application. It will use the same base database but is currently being written in Python 3. 

Late 2020 to early 2021
Incorporated all the database mods to handle multiple species into a new project AnimalTrakker. Also moved all the desktop code into AnimalTrakker instead of LambTracker.

2021-2022
All development going into the new AnimalTrakker program to handle multiple species. Full database redesign done and initial AT mobile and desktop apps are working but not released yet. 

LambTracker is now in bug fix mode only. 
