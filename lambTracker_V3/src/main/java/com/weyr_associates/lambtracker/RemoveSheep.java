package com.weyr_associates.lambtracker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class RemoveSheep extends ListActivity  {

	private DatabaseHandler dbh;
	public Cursor 	cursor, cursor2;
	public Object	crsr, crsr2;
	public int 		nRecs, tempint, nrecs2, numsheep;
	public String mytoday;
	public Spinner remove_reason_spinner;
	public int which_remove_reason;
	public List<String> remove_reasons, remove_reasons_id_order, remove_reasons_display_order;
	public int 		thissheep_id;
	String     	cmd;
	Button btn;
	public SimpleCursorAdapter myadapter;
	ArrayAdapter<String> dataAdapter;
	private TextView Output;
    private Button changeDate;
    static final int DATE_PICKER_ID = 1111;
    private int year;
    private int month;
    private int day;
    public String removedate, deathdate;
    public List<String> test_names;
    public List<Integer> test_sheep_id;
    public SparseBooleanArray sp;
    ListView sheep_name_list;
    ListView test_name_list;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.remove_sheep);
		String 	dbfile = getString(R.string.real_database_file) ;
        Log.i("RemoveSheep", " after get database file");
    	dbh = new DatabaseHandler( this, dbfile );
//    	Get the date and time to add to the sheep record these are strings not numbers
    	mytoday = Utilities.TodayIs(); 

//		Set the Update Database button to be green
		btn = (Button) findViewById( R.id.update_database_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));

//		sheep_name_list = (ListView) findViewById(R.id.sheep_names);
		test_name_list = (ListView) findViewById(android.R.id.list);
		
//		Now go get all the current sheep names and format them
		cmd = String.format( "select sheep_table.sheep_id as _id, flock_prefix_table.flock_name, sheep_table.sheep_name " +
				" from sheep_table inner join flock_prefix_table " +
				"on flock_prefix_table.flock_prefixid = sheep_table.id_flockprefixid" +
				" where (sheep_table.death_date = '') "+
				"order by sheep_table.sheep_name asc ");  	        	
		Log.i("format record", " command is  " + cmd);
		crsr = dbh.exec( cmd );
		cursor   = ( Cursor ) crsr; 
		nRecs    = cursor.getCount();
		Log.i("RemoveSheep", " nRecs is " + String.valueOf(nRecs));
		cursor.moveToFirst();	
		test_names = new ArrayList<String>(); 
       	test_sheep_id = new ArrayList<Integer>();
       	numsheep = 0;
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
			Log.i("RemoveSheep",cursor.getString(2) );
			// Now go get the last location
			cmd = String.format( "select sheep_id, to_id_contactsid from sheep_location_history_table " +
					"where sheep_id = '%s' order by movement_date ", cursor.getInt(0));
			Log.i("get movement record ", " command is  " + cmd);
			crsr2 = dbh.exec( cmd );
			cursor2   = ( Cursor ) crsr2;
			nrecs2 = cursor2.getCount();
			if (nrecs2 > 0) {
				cursor2.moveToLast();
				tempint = cursor2.getInt(1);
				// set to look for Garvin Mesa location
				// TODO: 3/2/19 fix to handle all locations
				if (tempint == 1) {
					test_names.add(cursor.getString(1) + " " + cursor.getString(2));
					test_sheep_id.add(cursor.getInt(0));
					numsheep = numsheep + 1;
				}
				;
			};
		}
		Log.i("got ", " sheep here " + String.valueOf(numsheep));
		cursor.moveToFirst();	
		final ListView sheep_name_list = (ListView) findViewById(android.R.id.list);
		if (nRecs > 0) {

	        ArrayAdapter<String> adapter = (new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice,test_names));
	        test_name_list.setAdapter(adapter);
	        test_name_list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
	        
	        sheep_name_list.setOnItemClickListener(new OnItemClickListener(){
	            public void onItemClick(AdapterView<?> parent, View view,int position,long id) {
	                View v = sheep_name_list.getChildAt(position);
	                Log.i("in click","I am inside onItemClick and position is:"+String.valueOf(position));
	            }
	        });
	        
	        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
	     
	        sp=getListView().getCheckedItemPositions();
		}  		
		else {
			// No sheep data - publish an empty list to clear sheep names
			Log.i("LookForSheep", "no current sheep");
//			myadapter = new SimpleCursorAdapter(this, R.layout.list_entry_names, null, null, null, 0);
//			setListAdapter(myadapter);
		} 	
		// Fill the Remove Reason Spinner
		remove_reason_spinner = (Spinner) findViewById(R.id.remove_reason_spinner);
    	remove_reasons = new ArrayList<String>();
		remove_reasons_id_order = new ArrayList<String>();
		remove_reasons_display_order = new ArrayList<String>();
    	// Select All fields from remove reasons to build the spinner
        cmd = "select * from death_reason_table order by death_reason_display_order";
        crsr = dbh.exec( cmd );  
        cursor   = ( Cursor ) crsr;
    	dbh.moveToFirstRecord();
    	remove_reasons.add("Death Reason");
		remove_reasons_id_order.add("death reasons id order");
		remove_reasons_display_order.add("death reasons display order");
         // looping through all rows and adding to list
    	for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
			remove_reasons_id_order.add(cursor.getString(0));
			remove_reasons.add(cursor.getString(1));
			remove_reasons_display_order.add(cursor.getString(2));
    	}
    	
    	// Creating adapter for spinner
    	dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, remove_reasons);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		remove_reason_spinner.setAdapter (dataAdapter);
		remove_reason_spinner.setSelection(0);
		
		//	Set the date picker stuff here	
		Output = (TextView) findViewById(R.id.Output);
        
     // Get current date by calender       
        final Calendar c = Calendar.getInstance();
        year  = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day   = c.get(Calendar.DAY_OF_MONTH);
 
        // Show current date        
        Output.setText(new StringBuilder()
                // Month is 0 based, just add 1
        		.append(year).append("-").append(Utilities.Make2Digits(month + 1)).append("-").append(Utilities.Make2Digits(day)));
        removedate = String.valueOf(Output.getText());
   }
	public void changeDatePicker (View v) {                
        // On button click show datepicker dialog 
        showDialog(DATE_PICKER_ID);

    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DATE_PICKER_ID:
             
            // open datepicker dialog. 
            // set date picker for current date 
            // add pickerListener listener to date picker
            return new DatePickerDialog(this, pickerListener, year, month, day);
        }
        return null;
    }
 
    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {
        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker view, int selectedYear,
               int selectedMonth, int selectedDay) {
            
            year  = selectedYear;
            month = selectedMonth;
            day   = selectedDay;
 
            // Show selected date 
            Output.setText(new StringBuilder()
            // Month is 0 based, just add 1
    		.append(year).append("-").append(Utilities.Make2Digits(month + 1)).append("-").append(Utilities.Make2Digits(day)));
            removedate = String.valueOf(Output.getText());
        	}
        };
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id){
	           Log.d(getLocalClassName(), "onItemClick(" + arg1 + ","
	                    + position + "," + id + ")");
	            ListView lv = (ListView) arg0;
	            if (lv.isItemChecked(position)){
	            	Log.i("RemoveSheep", "Got a checked item");}
	            else{
	            	Log.i("RemoveSheep", "removed a check");
	            }  
		}
		public void updateDatabase( View v ){
			Integer temp1, ownership_change_reason;
			String death_reason, from_id_contactsid , to_id_contactsid;
			//	Get the selected remove reason from the spinner
			remove_reason_spinner = (Spinner) findViewById(R.id.remove_reason_spinner);
	    	which_remove_reason = remove_reason_spinner.getSelectedItemPosition();
//	    	removedate has the string of the remove date I want to use
	    	//	Set the death date to be the remove date
	    	deathdate = removedate;
			from_id_contactsid = "";
			to_id_contactsid = "";
	    	//	Check for the remove reason being died
	    	//  TODO
	    	//	This needs to be fixed in case people set up their remove reasons differently
	    	//	Maybe search for died in the string of the reason first?
	    	//	Currently hard coded from the remove_reason_table from us

			Log.i("removeSheep", " got to removeSheep with remove reason of " + String.valueOf(which_remove_reason));
			Log.i("removeSheep", " got to removeSheep with Tag position of " + remove_reason_spinner.getSelectedItemPosition());
			death_reason = String.valueOf(remove_reason_spinner.getSelectedItemPosition());
			Log.i("removeSheep", " got to removeSheep with Tag spinner number of " + death_reason);
			temp1 = Integer.valueOf(death_reason);
			death_reason = remove_reasons_id_order.get(temp1);
			Log.i("removeSheep", " got to removeSheep with Tag type id order number of " + death_reason);

			temp1 = Integer.valueOf(death_reason);
			Log.i("removeSheep", " got to removeSheep with Tag type id number of " + String.valueOf(temp1));
			// set ownership to blank
			ownership_change_reason = 2;
			// set locations
	    	switch (temp1){
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
					// on farm deaths so set from_id_contactsid to Garvin and to_id_contactsid to blank
					// TODO: 3/2/19 fix to handle multiple locations
					from_id_contactsid = "1";
					to_id_contactsid = "";
					break;
				case 8:
					// 	Sheep butchered so need to get a slaughter location
					//  set from_id_contactsid to Garvin and to_id_contactsid to Homestead
					// TODO: 3/2/19 fix to handle multiple locations
					from_id_contactsid = "1";
					to_id_contactsid = "89";
					break;
				case 9:
				case 10:
				case 11:
					// on farm deaths so set from_id_contactsid to Garvin and to_id_contactsid to blank
					// TODO: 3/2/19 fix to handle multiple locations
					from_id_contactsid = "1";
					to_id_contactsid = "";
					break;

	    	} // end of case switch   	
	    	Log.i ("before loop", "remove date  " + removedate);
	    	Log.i ("before loop", "death date  " + deathdate);
	    	
	    	//	Now need to loop through all the sheep and update the sheep_table
	    	//	Set the death date and remove dates and clear all alerts for this sheep
	        boolean temp_value;
	        int temp_location, temp_size;
	        temp_size = sp.size();
	        Log.i ("before loop", "the sp size is " + String.valueOf(temp_size));
	    	for (int i=0; i<temp_size; i++){
	    		temp_value = sp.valueAt(i);
	    		temp_location = sp.keyAt(i);
	    		if (temp_value){
	    			Log.i ("for loop", "the sheep " + " " + test_names.get(temp_location)+ " is checked");
	    			Log.i ("for loop", "the sheep id is " + String.valueOf(test_sheep_id.get(temp_location)));
	    	    	// This needs to be in a loop for all sheep_id s that we found. Setting each one to be thissheep_id
//	    			cmd = String.format("update sheep_table set alert01 = '', id_locationid = '', management_group = '', death_date = '%s', remove_date = '%s', " +
//	    	    			"remove_reason = '%s' where sheep_id =%d ", deathdate, removedate, which_remove_reason,
//	    	    			test_sheep_id.get(temp_location) ) ;
					cmd = String.format("update sheep_table set alert01 = '', management_group = '', death_date = '%s'  " +
									", death_reason = '%s' where sheep_id =%d ", deathdate, death_reason,
							test_sheep_id.get(temp_location) ) ;
					Log.i("remove sheep ", "before cmd " + cmd);
	    			dbh.exec( cmd );
	    			Log.i("remove sheep ", "after cmd " + cmd);
					// on farm death create a location change to blank
					//set for Garvin only right now
					cmd = String.format("insert into sheep_location_history_table (sheep_id, movement_date,  " +
									"from_id_contactsid, to_id_contactsid ) values ('%s', '%s', '%s','%s') ", test_sheep_id.get(temp_location), deathdate
							,from_id_contactsid , to_id_contactsid ) ;
					Log.i("remove sheep ", "before cmd " + cmd);
					dbh.exec( cmd );
					Log.i("remove sheep ", "after cmd " + cmd);

				}
	    	}// for loop
	    	// Now need to go back 
			try { 
				cursor.close();
			}
			catch (Exception e) {
//				Log.i("Back Button", " In catch stmt cursor");  
				// In this case there is no adapter so do nothing
			}
	       	dbh.closeDB();  	
	    	finish();		
		}
}