package com.weyr_associates.lambtracker;

		import java.util.ArrayList;
		import java.util.List;
		import android.app.AlertDialog;
		import android.app.ListActivity;
		import android.content.ComponentName;
		import android.content.Context;
		import android.content.DialogInterface;
		import android.content.Intent;
		import android.content.ServiceConnection;
		import android.content.SharedPreferences;
		import android.database.Cursor;
		import android.database.sqlite.SQLiteException;
		import android.graphics.LightingColorFilter;
		import android.os.Bundle;
		import android.os.Handler;
		import android.os.IBinder;
		import android.os.Message;
		import android.os.Messenger;
		import android.os.RemoteException;
		import android.preference.PreferenceManager;
		import android.text.TextUtils;
		import android.util.Log;
		import android.view.LayoutInflater;
		import android.view.View;
		import android.view.inputmethod.InputMethodManager;
		import android.widget.ArrayAdapter;
		import android.widget.Button;
		import android.widget.CheckBox;
		import android.widget.EditText;
		import android.widget.ScrollView;
		import android.widget.SimpleCursorAdapter;
		import android.widget.Spinner;
		import android.widget.TableLayout;
		import android.widget.TableRow;
		import android.widget.TextView;
		import android.widget.Toast;

public class TissueSample extends ListActivity {
	private DatabaseHandler dbh;
	int             id;
	public int 		thissheep_id, thissire_id, thisdam_id;
	int             fedtagid, farmtagid, eidtagid;

	public String 	eidText, alert_text;
	public String 	thissire_name, thisdam_name;

	public Cursor cursor, cursor2, cursor3, cursor4, tagtypecursor, sheepcursor, tempcursor;
	public Object 	crsr, crsr2, crsr3, crsr4, tagcrsr, sheepcrsr, tempcrsr;
	public Spinner tag_type_spinner, tag_location_spinner, tag_color_spinner ;

	public List<String> tag_types_display_order, tag_types_id_order;
	public List<String> tag_types, tag_locations, tag_colors;


	public List<String> tissue_tests;
	public List<Integer> tissue_test_id;
	public String which_tissue_test;

	public CheckBox boxtissue;
	public CheckBox boxblood;
	public CheckBox boxweight;
	public Spinner sample_type_spinner, sample_test_spinner, sample_container_type_spinner;
	public List<String> sample_type, sample_type_id_order, sample_type_display_order;
	public List<String> sample_test, sample_test_id_order, sample_test_display_order;
	public List<String> sample_container_type, sample_container_type_display_order;
	public List<Integer> sample_type_id, sample_test_id, sample_container_type_id;
	public int which_tissue, which_test, which_container;
	public String current_container_id;
	public int id_tissuesampleid, which_testid, which_containerid;

	public String note_text;
	public int predefined_note01;
	public int             nRecs, numtests;
	private int			    recNo;
	public int temp_integer;
	private String LabelText = "";
	private String EID = "";
	private String BAA = "";
	private String SheepName = "";
	private Boolean AutoPrint = false;

	ArrayAdapter<String> dataAdapter;
	String     	cmd;
	Integer 	i;
	public Button btn, btn2;

	public SimpleCursorAdapter myadapter ;

	Messenger mService = null;
	Messenger mScaleService = null;
	Messenger mBaaCodeService = null;
	boolean mIsBound;
	boolean mIsScaleBound;
	boolean mIsBAABound;

	final Messenger mMessenger = new Messenger(new TissueSample.IncomingHandler());
	// variable to hold the string
	public String LastEID;
	private String LastBAA;
	private String Weight;

	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case eidService.MSG_UPDATE_STATUS:
					Bundle b1 = msg.getData();
//					Log.i("Tissue", "Got stat Message");
					break;
				case eidService.MSG_NEW_EID_FOUND:
					Bundle b2 = msg.getData();
					LastEID = (b2.getString("info1"));
//				We have a good whole EID number
					gotEID(null);
					break;
				case scaleService.MSG_NEW_SCALE_FOUND:
					Bundle b3 = msg.getData();
					Weight = (b3.getString("info1"));
//				Log.i("SheepManagement", "Got Weight.");
//				We have a good weight
					gotWeight(null);
					break;
				case baacodeService.MSG_NEW_Baacode_FOUND:
					Bundle b4 = msg.getData();
					LastBAA = (b4.getString("info1"));
				Log.i("Tissue", "Got BaaCode.");
//				We have a good baacode
					gotBAA(null);
					break;
				case eidService.MSG_UPDATE_LOG_APPEND:
					break;

				case baacodeService.MSG_UPDATE_LOG_APPEND:
					Log.i("Tissue", "Baacode data received");
					Bundle b5 = msg.getData();
					Log.i("Tissue", "BaaCode logappend.");
					break;

				case eidService.MSG_UPDATE_LOG_FULL:
//				Log.i("Evaluate ", "Log Full.");
					break;
				case scaleService.MSG_UPDATE_LOG_FULL:
					break;
				case baacodeService.MSG_UPDATE_LOG_FULL:
					break;
				case eidService.MSG_THREAD_SUICIDE:
//				Log.i("Evaluate", "Service informed Activity of Suicide.");
					doUnbindService();
					stopService(new Intent(TissueSample.this, eidService.class));
					break;
				default:
					super.handleMessage(msg);
			}
		}
	}

	private void LoadPreferences(Boolean NotifyOfChanges) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
//		Log.i("SheepManagement", "Load Pref.");
		try {
			String TextForLabel = preferences.getString("label", "text");
			LabelText = TextForLabel;
			AutoPrint = preferences.getBoolean("autop", false);
		} catch (NumberFormatException nfe) {
		}

	}
/// service connections eid scale baacode
/// dobind
/// dounbind

/// eid service connection & disconnection
	public ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			mService = new Messenger(service);
//			Log.i("SheepMgmt", "At Service.");
			try {
				//Register client with service
				Message msg = Message.obtain(null, eidService.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been unexpectedly disconnected - process crashed.
			mService = null;

		}
	};
///
/// scale service connection & disconnection
	public ServiceConnection mScaleConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			mScaleService = new Messenger(service);
//			Log.i("TissueSample", "At ScaleService.");
			try {
				//Register client with service
				Message msg = Message.obtain(null, scaleService.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mScaleService.send(msg);
			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been unexpectedly disconnected - process crashed.
			mScaleService = null;
		}
	};
///
/// baacode service connection & disconnection
	public ServiceConnection mbaacodeConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			mBaaCodeService = new Messenger(service);
			Log.i("TissueSample", "At baacodeService.");
			try {
				//Register client with service
				Message msg = Message.obtain(null, baacodeService.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mBaaCodeService.send(msg);
			} catch (RemoteException e) {
				Log.i("TissueSample", "At baacodeService exception.");
				// In this case the service has crashed before we could even do anything with it
			}
			//Update status
			try {
				Message msg = Message.obtain(null, baacodeService.MSG_UPDATE_STATUS, 0, 0);
				msg.replyTo = mMessenger;
				mBaaCodeService.send(msg);
			} catch (RemoteException e) {
				Log.i("TissueSample", "At baacodeService update status crash.");
				// In this case the service has crashed before we could even do anything with it
			}
			//Request full log from service.
			try {
				//Update status
				Message msg = Message.obtain(null, baacodeService.MSG_UPDATE_LOG_FULL, 0, 0);
				msg.replyTo = mMessenger;
				mBaaCodeService.send(msg);
			} catch (RemoteException e) {
				Log.i("TissueSample", "At baacodeService update log full crash.");
				// In this case the service has crashed before we could even do anything with it
			}


		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been unexpectedly disconnected - process crashed.
			mBaaCodeService = null;
		}
	};
///
/// done with service connections and disconnections
/// check if all services are running and start if not
	private void CheckIfServiceIsRunning() {
		//If the service is running when the activity starts, we want to automatically bind to it.
		if (eidService.isRunning()) {
			doBindService();
		} else {
			startService(new Intent(TissueSample.this, eidService.class));
			doBindService();
		}
		if (scaleService.isScaleRunning()) {
			doBindScaleService();
		} else {
			startService(new Intent(TissueSample.this, scaleService.class));
			doBindScaleService();
		}
		if (baacodeService.isBaaRunning()) {
		     doBindbaacodeService();
		} else {
			Log.i("TissueSample", "At baacodeService start service");
			startService(new Intent(TissueSample.this, baacodeService.class));
			Log.i("TissueSample", "At baacodeService service started");
			doBindbaacodeService();
			Log.i("TissueSample", "At baacodeService service bound");
	}
	}
/// binding and unbinding of services eid scale baacode
/// eid bind
	void doBindService() {
		// Establish a connection with the service.  We use an explicit
		// class name because there is no reason to be able to let other
		// applications replace our component.

		bindService(new Intent(this, eidService.class), mConnection, Context.BIND_AUTO_CREATE);

		mIsBound = true;

		if (mService != null) {

			try {
				//Request status update
				Message msg = Message.obtain(null, eidService.MSG_UPDATE_STATUS, 0, 0);
				msg.replyTo = mMessenger;
				mService.send(msg);
//				Log.i("SheepMgmt", "At doBind4.");
				//Request full log from service.
				msg = Message.obtain(null, eidService.MSG_UPDATE_LOG_FULL, 0, 0);
				mService.send(msg);
			} catch (RemoteException e) {
			}
		}

	}
/// eid unbind
	void doUnbindService() {
//		Log.i("TissueSample", "At DoUnbindservice");
		if (mService != null) {
			try {
			//Stop eidService from sending tags
				Message msg = Message.obtain(null, eidService.MSG_NO_TAGS_PLEASE);
				msg.replyTo = mMessenger;
				mService.send(msg);

			} catch (RemoteException e) {
			// In this case the service has crashed before we could even do anything with it
			}
		}
		if (mIsBound) {
		// If we have received the service, and hence registered with it, then now is the time to unregister.
			if (mService != null) {
				try {
				Message msg = Message.obtain(null, eidService.MSG_UNREGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);
				} catch (RemoteException e) {
				// There is nothing special we need to do if the service has crashed.
				}
			}
		// Detach our existing connection.
		unbindService(mConnection);
		mIsBound = false;
	}
}
/// scale bind

	void doBindScaleService() {
		// Establish a connection with the service.  We use an explicit
		// class name because there is no reason to be able to let other
		// applications replace our component.

		bindService(new Intent(this, scaleService.class), mScaleConnection, Context.BIND_AUTO_CREATE);

		mIsScaleBound = true;
	}

/// scale unbind
	void doUnbindScaleService() {
		if (mScaleService != null) {
			try {
				//Stop scaleService from sending tags
				Message msg = Message.obtain(null, scaleService.MSG_NO_TAGS_PLEASE);
				msg.replyTo = mMessenger;
				mScaleService.send(msg);

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}
		if (mIsScaleBound) {
			// If we have received the service, and hence registered with it, then now is the time to unregister.
			if (mScaleService != null) {
				try {
					Message msg = Message.obtain(null, scaleService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = mMessenger;
					mScaleService.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service has crashed.
				}
			}
			// Detach our existing connection.
			unbindService(mScaleConnection);
			mIsScaleBound = false;
		}
	}

/// baacode bind
void doBindbaacodeService() {
	// Establish a connection with the service.  We use an explicit
	// class name because there is no reason to be able to let other
	// applications replace our component.
	Log.i("TissueSample", "At baacode bind1.");
	bindService(new Intent(this, baacodeService.class), mbaacodeConnection, Context.BIND_AUTO_CREATE);

	mIsBAABound = true;

	if (mBaaCodeService != null) {

		try {
			//Request status update
			Message msg = Message.obtain(null, baacodeService.MSG_UPDATE_STATUS, 0, 0);
			msg.replyTo = mMessenger;
			mBaaCodeService.send(msg);
//			Log.i("TissueSample", "At baacode bind2.");
			//Request full log from service.
			msg = Message.obtain(null, baacodeService.MSG_UPDATE_LOG_FULL, 0, 0);
			mBaaCodeService.send(msg);
		} catch (RemoteException e) {
//			Log.i("Tissue", "message exception bind");
		}
	}

}
/// baacode unbind
	void doUnbindbaacodeService() {
		Log.i("TissueSample", "At baacodeUnbindservice");
		if (mBaaCodeService != null) {
			try {
				//Stop baacodeService from sending tags
				Message msg = Message.obtain(null, baacodeService.MSG_NO_TAGS_PLEASE);
				msg.replyTo = mMessenger;
				Log.i("TissueSample", "At baacode unbind.");
				mBaaCodeService.send(msg);
			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}
		if (mIsBAABound) {
			// If we have received the service, and hence registered with it, then now is the time to unregister.
			if (mBaaCodeService != null) {
				try {
					Message msg = Message.obtain(null, baacodeService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = mMessenger;
					mBaaCodeService.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service has crashed.
				}
			}
			// Detach our existing connection.
			unbindService(mbaacodeConnection);
			mIsBAABound = false;
		}
	}

/// end bind & unbinds

	// use EID reader to look up a sheep
	public void gotEID(View v) {
		//	make the scan eid button red
		btn = (Button) findViewById(R.id.scan_eid_btn);
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
//		String eid = this.getIntent().getExtras().getString("com.weyr_associates.lambtracker.LastEID");
//    	Log.i("TissueSample", " before input text " + eid);
//    	Log.i("TissueSample", " before input text " + LastEID);
		// 	Display the EID number
		TextView TV = (TextView) findViewById(R.id.inputText);
		TV.setText(LastEID);
		Log.i("in gotEID ", "with LastEID of " + LastEID);
		btn2 = (Button) findViewById(R.id.look_up_sheep_btn);
		btn2.performClick();
	}

	// use scale to weigh the sheep
	public void gotWeight(View v) {
		// make the scan weight button red
		btn = (Button) findViewById(R.id.scan_sample_container_btn);
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));

		// 	Display the Weight number
		TextView TV = (TextView) findViewById(R.id.trait11_data);
		TV.setText(Weight);
		Log.i("in gotWeight ", "with weight of " + Weight);
	}

	// use BaaCode reader
	public void gotBAA(View v) {
		//	make the scan sample container button red
		btn = (Button) findViewById(R.id.scan_sample_container_btn);
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));

		// 	Display the BaaCode number
		TextView TV = (TextView) findViewById(R.id.sample_container_id);
		TV.setText(LastBAA);
		Log.i("in gotBAA ", "with LastBAA of " + LastBAA);

	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tissue_test);
 //       Log.i("TissueSample", " after set content view");
//        View v = null;
		String 	dbfile = getString(R.string.real_database_file) ;
//        Log.i("TissueSample", " after get database file");
		dbh = new DatabaseHandler( this, dbfile );
//		Added the variable definitions here
		String          cmd;

//		CheckIfServiceIsRunning();
		LoadPreferences (true);
		Log.i("TissueSample", "back from isRunning");

		thissheep_id = 0;
		// Fill the Tag Type Spinner
		tag_type_spinner = (Spinner) findViewById(R.id.tag_type_spinner);
		tag_types = new ArrayList<String>();
		tag_types_id_order= new ArrayList<String>();
		tag_types_display_order = new ArrayList<String>();
		// Select All fields from id types to build the spinner
		cmd = "select * from id_type_table order by id_type_display_order";
//		Log.i("fill tag spinner", "command is " + cmd);
		tagcrsr = dbh.exec( cmd );
		tagtypecursor   = ( Cursor ) tagcrsr;
		dbh.moveToFirstRecord();
		tag_types.add("Select a Type");
		tag_types_id_order.add("Tag_types_id");
		tag_types_display_order.add("tag_type_display_order");
		Log.i("fill tag spinner", "added first option ");
		// looping through all rows and adding to list
		for (tagtypecursor.moveToFirst(); !tagtypecursor.isAfterLast(); tagtypecursor.moveToNext()){
			tag_types_id_order.add(tagtypecursor.getString(0));
			Log.i("TissueSample", "tag_types_id_order " + tagtypecursor.getString(0));
			tag_types.add(tagtypecursor.getString(1));
			Log.i("TissueSample", "tag_type name " + tagtypecursor.getString(1));
			tag_types_display_order.add(tagtypecursor.getString(3));
			Log.i("TissueSample", "tag_types_display_order " + tagtypecursor.getString(3));
		}

		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, tag_types);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tag_type_spinner.setAdapter (dataAdapter);
		//	set initial tag type to look for to be electronic it's based on the display order
		// TODO: 2/25/19
		tag_type_spinner.setSelection(4);

		Log.i("TissueSample", " start of fill out the tissue sampling stuff ");
		//	Get set up to fill various spinners
		//	Fill the sample Type Spinner
		sample_type_spinner = (Spinner) findViewById(R.id.sample_type_spinner);
		Log.i("TissueSample", " after get the place to fill the spinner ");
		sample_type = new ArrayList<String>();
		sample_type_id = new ArrayList<Integer>();
		sample_type_id_order = new ArrayList<String>();
		sample_type_display_order = new ArrayList<String>();
		Log.i("TissueSample", " after create the list variable instances ");
		cmd = String.format("select id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_display_order  " +
				" from tissue_sample_type_table order by tissue_sample_type_display_order ASC ");
		Log.i("tissue types", "command is " + cmd);
		crsr = dbh.exec(cmd);
		Log.i("tissue types", "after run the command  " + cmd);
		cursor = (Cursor) crsr;
		Log.i("tissue types", "after set the cursor  ");
		dbh.moveToFirstRecord();
		sample_type.add("Select a Sample Type");
		Log.i("tissue types", "after add sample type  " + cmd);
		sample_type_id.add(0);
		Log.i("tissue types", "after add sample type ID " + cmd);
		sample_type_id_order.add("sample_type_id_order");
		Log.i("tissue types", "after add sample type order  ");
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			Log.i("TissueSample", " adding tissue id " + cursor.getInt(0));
			sample_type_id.add(cursor.getInt(0));
			Log.i("TissueSample", " adding tissue type " + cursor.getString(1));
			sample_type.add(cursor.getString(1));
			Log.i("TissueSample", " in loop fill spinner after get sample type ");
			sample_type_id_order.add(cursor.getString(2));
			Log.i("TissueSample", " in loop fill spinner after get sample type display order ");

		}

		Log.i("TissueSample", " after filling tissue sample type spinner");
		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sample_type);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sample_type_spinner.setAdapter(dataAdapter);
		if (cursor.getCount() > 0) {
			// this needs to be a preference
			sample_type_spinner.setSelection(2);
		}
		Log.i("TissueSample", " after display tissue type spinner");


	//	Fill the test selection spinner
		sample_test_spinner = (Spinner) findViewById(R.id.sample_test_spinner);
		sample_test_id = new ArrayList<Integer>();
		sample_test_display_order = new ArrayList<String>();
		sample_test = new ArrayList<String>();
		// Select All fields from test options to build the spinner
		cmd = "select id_tissuetestid, tissue_test_display_order, tissue_test_abbrev from tissue_test_table order by tissue_test_display_order";
		Log.i("fill sample spinner", "command is " + cmd);
		crsr = dbh.exec(cmd);
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
		sample_test_id.add(0);
		sample_test_display_order.add("sample_test_display_order");
		sample_test.add("Select a Test");
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			sample_test_id.add(cursor.getInt(0));
			sample_test_display_order.add(cursor.getString(1));
			Log.i("TissueSample", " after add to tissue test display spinner " + cursor.getString(1));
			sample_test.add(cursor.getString(2));
			Log.i("TissueSample", " tissue test " + cursor.getString(2));
		}
		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sample_test);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sample_test_spinner.setAdapter(dataAdapter);
		sample_test_spinner.setSelection(5);

//	Fill the container selection spinner
		sample_container_type_spinner = (Spinner) findViewById(R.id.sample_container_type_spinner);
		sample_container_type = new ArrayList<String>();
		sample_container_type_id = new ArrayList<Integer>();
		// sample_container_type_id_order = new ArrayList<String>();
		sample_container_type_display_order = new ArrayList<String>();
		// Select All fields from test options to build the spinner
		cmd = "select id_tissuecontainertypeid, tissue_sample_container_display_order, tissue_sample_container_name " +
				"from tissue_sample_container_type_table order by tissue_sample_container_display_order";
		Log.i("fill container spinner", "command is " + cmd);
		crsr = dbh.exec(cmd);
		cursor = (Cursor) crsr;
		dbh.moveToFirstRecord();
		sample_container_type.add("Select a Container");
		sample_container_type_id.add(0);
		sample_container_type_display_order.add("sample_container_type_display_order");
		// looping through all rows and adding to list
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			sample_container_type.add(cursor.getString(2));
			Log.i("TissueSample", " container " + cursor.getString(2));
			sample_container_type_id.add(cursor.getInt(0));
			Log.i("TissueSample", " tissue test id spinner " + cursor.getString(0));
			sample_container_type_display_order.add(cursor.getString(1));
			Log.i("TissueSample", " tissue test display spinner " + cursor.getString(1));
		}
		// Creating adapter for spinner
		Log.i("TissueSample", " before get container spinner " );
		dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sample_container_type);
		Log.i("TissueSample", " after get container spinner " );
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sample_container_type_spinner.setAdapter(dataAdapter);
		Log.i("TissueSample", " after set container spinner adapter" );
		sample_container_type_spinner.setSelection(3);
		Log.i("TissueSample", " after set container selection " );

		// make the alert button normal and disabled
		btn = (Button) findViewById( R.id.alert_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
		btn.setEnabled(false);
		Log.i("TissueSample", " after disable alert button " );
		//	Disable the Next Record and Prev. Record button until we have multiple records
		btn = (Button) findViewById( R.id.next_rec_btn );
		btn.setEnabled(false);
		btn = (Button) findViewById( R.id.prev_rec_btn );
		btn.setEnabled(false);

		//	make the eid button red
		btn = (Button) findViewById( R.id.scan_eid_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));

		//	make the scan TSU button red
		btn = (Button) findViewById( R.id.scan_sample_container_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));

		//	make the take weight button red
		btn = (Button) findViewById( R.id.scan_weight_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
	}

	public void lookForSheep (View v){
		Boolean exists;
		Integer temp1;
		String temp2;
		TextView TV;
		exists = true;
		// Hide the keyboard when you click the button
		InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
		//	Disable the Next Record and Prev. Record button until we have multiple records
		btn = (Button) findViewById( R.id.next_rec_btn );
		btn.setEnabled(false);
		btn = (Button) findViewById( R.id.prev_rec_btn );
		btn.setEnabled(false);

		TV = (EditText) findViewById( R.id.inputText );
		String	tag_num = TV.getText().toString();

		Log.i("LookForSheep", " got to lookForSheep with Tag Number of " + String.valueOf(tag_num));
		Log.i("LookForSheep", " got to lookForSheep with Tag position of " + tag_type_spinner.getSelectedItemPosition());
		temp2 = String.valueOf(tag_type_spinner.getSelectedItemPosition());
		Log.i("LookForSheep", " got to lookForSheep with Tag spinner number of " + temp2);
		temp1 = Integer.valueOf(temp2);
		temp2 = tag_types_id_order.get(temp1);
		Log.i("LookForSheep", " got to lookForSheep with Tag type id order number of " + temp2);

		temp1 = Integer.valueOf(temp2);
		Log.i("LookForSheep", " got to lookForSheep with Tag type id number of " + String.valueOf(temp1));
//        String	tag_num = TV.getText().toString();
//
//        Log.i("LookForSheep", " got to lookForSheep with Tag Number of " + tag_num);
//        Log.i("LookForSheep", " got to lookForSheep with Tag type of " + tag_type_spinner.getSelectedItemPosition());
		exists = tableExists("sheep_table");
		if (exists){
			//			switch (tag_type_spinner.getSelectedItemPosition()){
			switch (temp1){
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					if( tag_num != null && tag_num.length() > 0 ){
						// Get the sheep id from the id table for this tag number and selected tag type
//    						cmd = String.format( "select sheep_id from id_info_table where tag_number='%s' "+
//				        			"and id_info_table.tag_type='%s' and (id_info_table.tag_date_off is null or" +
//				        			" id_info_table.tag_date_off = '') "
//				        			, tag_num , tag_type_spinner.getSelectedItemPosition());
						cmd = String.format( "select sheep_id from id_info_table where tag_number='%s' "+
										"and id_info_table.tag_type='%s' "
								, tag_num , temp1);
						Log.i("searchByNumber", "command is " + cmd);
						sheepcrsr = dbh.exec( cmd );
						sheepcursor   = ( Cursor ) sheepcrsr;
						recNo    = 1;
						nRecs    = sheepcursor.getCount();
						Log.i("searchByNumber", " nRecs = "+ String.valueOf(nRecs));
						dbh.moveToFirstRecord();
						Log.i("searchByNumber", " the cursor is of size " + String.valueOf(dbh.getSize()));
						if( dbh.getSize() == 0 ){
							// no sheep with that  tag in the database so clear out and return
							clearBtn( v );
							TV = (TextView) findViewById( R.id.sheepnameText );
							TV.setText( "Cannot find this sheep." );
							return;
						}
						thissheep_id = dbh.getInt(0);
						Log.i("searchByNumber", "This sheep is record " + String.valueOf(thissheep_id));
						if (nRecs >1){
							//	Have multiple sheep with this tag so enable next button
							btn = (Button) findViewById( R.id.next_rec_btn );
							btn.setEnabled(true);
						}
						Log.i("searchByNumber", " Before formatting the record");
						//	We need to call the format the record method
						formatSheepRecord(v);
					}
					break;
				case 6:
					//	got a split
					//	Assume no split ears at this time.
					//	Needs modification for future use
//				    	TV = (TextView) findViewById( R.id.sheepnameText );
//			        	TV.setText( "Cannot search on splits yet." );
//				    	// TODO
//				        break;
					if( tag_num != null && tag_num.length() < 1 ){
						// Get the sheep id from the id table for split ears
						cmd = String.format( "select sheep_id from id_info_table where tag_number='%s' "+
										"and id_info_table.tag_type='%s' "
								, tag_num , temp1);
						Log.i("searchBysplit", "command is " + cmd);
						sheepcrsr = dbh.exec( cmd );
						sheepcursor   = ( Cursor ) sheepcrsr;
						recNo    = 1;
						nRecs    = sheepcursor.getCount();
						Log.i("searchByNumber", " nRecs = "+ String.valueOf(nRecs));
						dbh.moveToFirstRecord();
						Log.i("searchByNumber", " the cursor is of size " + String.valueOf(dbh.getSize()));
						if( dbh.getSize() == 0 ){
							// no sheep with that  tag in the database so clear out and return
							clearBtn( v );
							TV = (TextView) findViewById( R.id.sheepnameText );
							TV.setText( "Cannot find this sheep." );
							return;
						}
						thissheep_id = dbh.getInt(0);
						Log.i("searchByNumber", "This sheep is record " + String.valueOf(thissheep_id));
						if (nRecs >1){
							//	Have multiple sheep with this tag so enable next button
							btn = (Button) findViewById( R.id.next_rec_btn );
							btn.setEnabled(true);
						}
						Log.i("searchByNumber", " Before formatting the record");
						//	We need to call the format the record method
						formatSheepRecord(v);
					}
					break;
				case 7:
					//	got a notch
					if( tag_num != null && tag_num.length() < 1 ){
						// Get the sheep id from the id table for split ears
						cmd = String.format( "select sheep_id from id_info_table where tag_number='%s' "+
										"and id_info_table.tag_type='%s' "
								, tag_num , temp1);
						Log.i("searchBysplit", "command is " + cmd);
						sheepcrsr = dbh.exec( cmd );
						sheepcursor   = ( Cursor ) sheepcrsr;
						recNo    = 1;
						nRecs    = sheepcursor.getCount();
						Log.i("searchByNumber", " nRecs = "+ String.valueOf(nRecs));
						dbh.moveToFirstRecord();
						Log.i("searchByNumber", " the cursor is of size " + String.valueOf(dbh.getSize()));
						if( dbh.getSize() == 0 ){
							// no sheep with that  tag in the database so clear out and return
							clearBtn( v );
							TV = (TextView) findViewById( R.id.sheepnameText );
							TV.setText( "Cannot find this sheep." );
							return;
						}
						thissheep_id = dbh.getInt(0);
						Log.i("searchByNumber", "This sheep is record " + String.valueOf(thissheep_id));
						if (nRecs >1){
							//	Have multiple sheep with this tag so enable next button
							btn = (Button) findViewById( R.id.next_rec_btn );
							btn.setEnabled(true);
						}
						Log.i("searchByNumber", " Before formatting the record");
						//	We need to call the format the record method
						formatSheepRecord(v);
					}
					break;
				case 8:
					//	got a name
					tag_num = "%" + tag_num + "%";
					// Modified this so I can look up removed sheep as well
					cmd = String.format( "select sheep_id, sheep_name from sheep_table where sheep_name like '%s'"
							, tag_num );
					Log.i("searchByName", "command is " + cmd);
					sheepcrsr = dbh.exec( cmd );
					sheepcursor   = ( Cursor ) sheepcrsr;
					recNo    = 1;
					nRecs    = sheepcursor.getCount();
					Log.i("searchByName", " nRecs = "+ String.valueOf(nRecs));
					dbh.moveToFirstRecord();
					Log.i("searchByName", " the cursor is of size " + String.valueOf(dbh.getSize()));
					if( dbh.getSize() == 0 )
					{ // no sheep with that name in the database so clear out and return
						clearBtn( v );
						TV = (TextView) findViewById( R.id.sheepnameText );
						TV.setText( "Cannot find this sheep." );
						return;
					}
					thissheep_id = dbh.getInt(0);
					if (nRecs >1){
						//	Have multiple sheep with this name so enable next button
						btn = (Button) findViewById( R.id.next_rec_btn );
						btn.setEnabled(true);
					}
					//	We need to call the format the record method
					formatSheepRecord(v);
					break;
			} // end of case switch
		}else {
			clearBtn( null );
			TV = (TextView) findViewById( R.id.sheepnameText );
			TV.setText( "Sheep Database does not exist." );
		}

	}

	public void formatSheepRecord (View v){
		TextView TV;

		thissheep_id = sheepcursor.getInt(0);
		Log.i("format record", "This sheep is record " + String.valueOf(thissheep_id));
		// we have a sheep so enable buttons and make green

		btn = (Button) findViewById( R.id.print_label_btn );
		btn.setEnabled(true);
		btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));
		btn = (Button) findViewById( R.id.update_database_btn );
		btn.setEnabled(true);
		btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));

//		Log.i("format record", " recNo = "+ String.valueOf(recNo));
		cmd = String.format( "select sheep_table.sheep_name, sheep_table.sheep_id, id_type_table.idtype_name, " +
				"tag_colors_table.tag_color_name, id_info_table.tag_number, id_location_table.id_location_abbrev, " +
				"id_info_table.id_infoid as _id, id_info_table.tag_date_off, sheep_table.alert01,  " +
				"sheep_table.sire_id, sheep_table.dam_id, sheep_table.birth_date, birth_type_table.birth_type," +
				"sex_table.sex_name, sheep_table.birth_weight " +
				"from sheep_table inner join id_info_table on sheep_table.sheep_id = id_info_table.sheep_id " +
				"inner join birth_type_table on id_birthtypeid = sheep_table.birth_type " +
				"inner join sex_table on sex_table.sex_sheepid = sheep_table.sex " +
				"left outer join tag_colors_table on id_info_table.tag_color_male = tag_colors_table.id_tagcolorsid " +
				"left outer join id_location_table on id_info_table.tag_location = id_location_table.id_locationid " +
				"inner join id_type_table on id_info_table.tag_type = id_type_table.id_typeid " +
				"where id_info_table.sheep_id ='%s' and (id_info_table.tag_date_off is null or " +
				"id_info_table.tag_date_off is '')order by idtype_name asc", thissheep_id);

		crsr= dbh.exec( cmd );
		cursor   = ( Cursor ) crsr;
		cursor.moveToFirst();
		TV = (TextView) findViewById( R.id.sheepnameText );
		TV.setText (dbh.getStr(0));
		SheepName = dbh.getStr(0);
		alert_text = dbh.getStr(8);

		//	Get the sire and dam id numbers
		thissire_id = dbh.getInt(9);
		Log.i("format record", " Sire is " + String.valueOf(thissire_id));
		thisdam_id = dbh.getInt(10);
		Log.i("format record", " Dam is " + String.valueOf(thisdam_id));

		//	Go get the sire name
		if (thissire_id != 0){
			cmd = String.format( "select sheep_table.sheep_name from sheep_table where sheep_table.sheep_id = '%s'", thissire_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec( cmd);
//	        Log.i("format record", " after second db lookup");
			cursor2   = ( Cursor ) crsr2;
			cursor2.moveToFirst();
			TV = (TextView) findViewById( R.id.sireName );
			thissire_name = dbh.getStr(0);
			TV.setText (thissire_name);
//			Log.i("format record", " Sire is " + thissire_name);
//	        Log.i("format record", " Sire is " + String.valueOf(thissire_id));
		}
		if(thisdam_id != 0){
			cmd = String.format( "select sheep_table.sheep_name from sheep_table where sheep_table.sheep_id = '%s'", thisdam_id);
			crsr3 = dbh.exec( cmd);
			cursor3   = ( Cursor ) crsr3;
			cursor3.moveToFirst();
			TV = (TextView) findViewById( R.id.damName );
			thisdam_name = dbh.getStr(0);
			TV.setText (thisdam_name);
//			Log.i("format record", " Dam is " + thisdam_name);
//	        Log.i("format record", " Dam is " + String.valueOf(thisdam_id));
		}
		try {
			Log.i("try block", " Before finding an electronic tag if it exists");
			cmd = String.format( "select sheep_table.sheep_id, id_type_table.idtype_name, " +
					"id_info_table.tag_number " +
					"from sheep_table inner join id_info_table on sheep_table.sheep_id = id_info_table.sheep_id " +
					"inner join id_type_table on id_info_table.tag_type = id_type_table.id_typeid " +
					"where id_info_table.sheep_id ='%s' and id_info_table.tag_date_off is null and " +
					"id_info_table.tag_type = 2 order by idtype_name asc", thissheep_id);
			crsr4 = dbh.exec( cmd );
			cursor4   = ( Cursor ) crsr4;
			cursor4.moveToFirst();
//		Log.i("getlastEID filled", "This sheep is id " + String.valueOf(dbh.getInt(0)));
//		Log.i("getlastEID filled", "This sheep id type is " + dbh.getStr(1));
			LastEID = dbh.getStr(2);
			Log.i("LastEID is ", dbh.getStr(2));
		}
		catch(Exception r)
		{
			LastEID = "000_000000000000";
			Log.v("fill LAST EID ", " in sheep management RunTimeException: " + r);
		}
//    	Log.i("FormatRecord", " before formatting results");
		//	Get set up to try to use the CursorAdapter to display all the tag data
		//	Select only the columns I need for the tag display section
		String[] fromColumns = new String[ ]{ "tag_number", "tag_color_name", "id_location_abbrev", "idtype_name"};
		Log.i("FormatRecord", "after setting string array fromColumns");
		//	Set the views for each column for each line. A tag takes up 1 line on the screen
		int[] toViews = new int[] { R.id.tag_number, R.id.tag_color_name, R.id.id_location_abbrev, R.id.idtype_name};
		Log.i("FormatRecord", "after setting string array toViews");
		myadapter = new SimpleCursorAdapter(this, R.layout.list_entry, cursor ,fromColumns, toViews, 0);
		Log.i("FormatRecord", "after setting myadapter");
		setListAdapter(myadapter);
		Log.i("FormatRecord", "after setting list adapter");

		// Now we need to check and see if there is an alert for this sheep
//	   	Log.i("Alert Text is " , alert_text);
//		Now to test of the sheep has an alert and if so then display the alert & set the alerts button to red
		if (alert_text != null && !alert_text.isEmpty() && !alert_text.trim().isEmpty()){
			// make the alert button red
			Button btn = (Button) findViewById( R.id.alert_btn );
			btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
			btn.setEnabled(true);
			//	testing whether I can put up an alert box here without issues
			showAlert(v);
		}
	}
	public void updateDatabase( View v ){

		TextView TV;
		String temp_string;
		Float trait11_data = 0.0f;
		Log.i("Update Database ", "before doing anything ");
		// Disable Update Database button and make it red to prevent getting 2 records at one time
		btn = (Button) findViewById( R.id.update_database_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
		btn.setEnabled(false);
		// If there is no sheep ID then drop out completely
		// thissheep_id is 0 if no sheep has been selected.
		if (thissheep_id != 0) {
			//	Get the date and time to enter into the database.
			String mytoday = Utilities.TodayIs();
			String mytime = Utilities.TimeIs();
			boxtissue = (CheckBox) findViewById(R.id.checkBoxTakeTissue);
			if (boxtissue.isChecked()) {
				//	Go get which sample type was selected in the spinner
				sample_type_spinner = (Spinner) findViewById(R.id.sample_type_spinner);
				which_tissue = sample_type_spinner.getSelectedItemPosition();
				Log.i("Update Database ", "sample type position " + which_tissue);
				which_tissue = Integer.valueOf(which_tissue);
				if (which_tissue == 0) {
					//	Need to require a value for tissue here
					//  Missing data so  display an alert
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(R.string.tissue_fill_fields)
							.setTitle(R.string.tissue_fill_fields);
					builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById(R.id.update_database_btn);
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
							btn.setEnabled(true);
							return;
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				} else {
					Log.i("tissue type", String.valueOf(which_tissue));
				}
				// bug crash
				Log.i("tissue spinner", " position is " + String.valueOf(which_tissue));
				i = sample_type_id.get(which_tissue);
				Log.i("sample type id", " value is " + String.valueOf(i));

				//	Go get which test was selected in the spinner
				sample_test_spinner = (Spinner) findViewById(R.id.sample_test_spinner);
				which_test = sample_test_spinner.getSelectedItemPosition();
				Log.i("Update Database ", "sample test position " + which_test);
				which_test = Integer.valueOf(which_test);
				if (which_test == 0) {
					//	Need to require a value for test here
					//  Missing data so  display an alert
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(R.string.test_fill_fields)
							.setTitle(R.string.test_fill_fields);
					builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById(R.id.update_database_btn);
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
							btn.setEnabled(true);
							return;
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				} else {
					Log.i("tissue test", String.valueOf(which_test));
				}
				Log.i("tissue spinner", " position is " + String.valueOf(which_test));
				which_testid = sample_test_id.get(which_test);
				Log.i("sample type id", " value is " + String.valueOf(which_testid));

				//	Go get which container was selected in the spinner
				sample_container_type_spinner = (Spinner) findViewById(R.id.sample_container_type_spinner);
				which_container = sample_container_type_spinner.getSelectedItemPosition();
				Log.i("Update Database ", "sample container type " + which_container);
				which_container = Integer.valueOf(which_container);
				if (which_container == 0) {
					//	Need to require a value for test here
					//  Missing data so  display an alert
					AlertDialog.Builder builder = new AlertDialog.Builder(this);
					builder.setMessage(R.string.test_fill_fields)
							.setTitle(R.string.test_fill_fields);
					builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int idx) {
							// User clicked OK button
							// make update database button normal and enabled so we can try again
							btn = (Button) findViewById(R.id.update_database_btn);
							btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
							btn.setEnabled(true);
							return;
						}
					});
					AlertDialog dialog = builder.create();
					dialog.show();
					return;
				} else {
					Log.i("tissue container", String.valueOf(which_container));
				}
				Log.i("container spinner", " position is " + String.valueOf(which_container));
				which_containerid = sample_container_type_id.get(which_container);
				Log.i("sample container id", " value is " + String.valueOf(which_containerid));

				//	Now we need to get the container ID
				TV = (TextView) findViewById(R.id.sample_container_id);
				current_container_id = TV.getText().toString();

				//	Start updating the database with the info we've collected
				Log.i("sample taken ", "Before add record to tissue_sample_taken_table db");
				cmd = String.format("insert into tissue_sample_taken_table (sheep_id, id_tissuesampletypeid, tissue_sample_date, " +
						"tissue_sample_time, id_tissuecontainertypeid, tissue_sample_container_id) values (%s, '%s', '%s', '%s', '%s', '%s') "
						,thissheep_id, which_tissue, mytoday, mytime, which_container, current_container_id);
				Log.i("sample taken ", "add to tissue_sample_taken_table db cmd is " + cmd);
				dbh.exec(cmd);
				Log.i("sample taken ", "after insert into tissue_sample_taken_table");

				// Need the id_tissuesampleid of that record
				cmd = String.format("select last_insert_rowid()");
				crsr = dbh.exec( cmd );
				cursor   = ( Cursor ) crsr;
				dbh.moveToFirstRecord();
				id_tissuesampleid = dbh.getInt(0);
				Log.i("tissue sample ", "the sample taken id is " + String.valueOf(id_tissuesampleid));

				//	Ready to update the test requested
				//	No lab is assigned at this time
				cmd = String.format("insert into tissue_test_request_table (id_tissuesampleid, id_tissuetestid " +
								" ) values (%s, '%s') "
						,id_tissuesampleid, which_testid);
				Log.i("sample taken ", "add to tissue_test_request_table db cmd is " + cmd);
				dbh.exec(cmd);
				Log.i("sample taken ", "after insert into tissue_test_request_table_table");

//	Take a weight if checked
				boxweight = (CheckBox) findViewById(R.id.checkBoxTakeWeight);
				if (boxweight.isChecked()) {
					//	get a sheep weight
					TV = (TextView) findViewById(R.id.trait11_data);
					temp_string = TV.getText().toString();
					if (TextUtils.isEmpty(temp_string)) {
						// EditText was empty
						// so no real data collected just break out
						trait11_data = 0.0f;
//			    			Log.i("save trait11", "float data is " + String.valueOf(trait11_data));
					} else {
						trait11_data = Float.valueOf(TV.getText().toString());
						Log.i("save trait11", "float data is " + String.valueOf(trait11_data));
					}
					// Calculate the age in days for this sheep for this evaluation to fill the age_in_days field
					cmd = String.format("Select julianday(birth_date) from sheep_table where sheep_id = '%s'", thissheep_id);
					Log.i("get birthdate eval ", cmd);
					dbh.exec(cmd);
					tempcrsr = dbh.exec(cmd);
					tempcursor = (Cursor) tempcrsr;
					dbh.moveToFirstRecord();
					temp_integer = (int) Utilities.GetJulianDate() - (dbh.getInt(0));
					Log.i("get age in days ", String.valueOf(temp_integer));

					//	go update the database with a sheep evaluation record for this weight and this sheep
					cmd = String.format("insert into sheep_evaluation_table (sheep_id, " +
									"trait_name01, trait_score01, trait_name02, trait_score02, trait_name03, trait_score03, " +
									"trait_name04, trait_score04, trait_name05, trait_score05, trait_name06, trait_score06," +
									"trait_name07, trait_score07, trait_name08, trait_score08, trait_name09, trait_score09, " +
									"trait_name10, trait_score10, trait_name11, trait_score11, trait_name12, trait_score12, " +
									"trait_name13, trait_score13, trait_name14, trait_score14, trait_name15, trait_score15, " +
									"trait_name16, trait_score16, trait_name17, trait_score17, trait_name18, trait_score18, " +
									"trait_name19, trait_score19, trait_name20, trait_score20, " +
									"trait_units11, trait_units12, trait_units13, trait_units14, trait_units15, eval_date, eval_time, age_in_days) " +
									"values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s," +
									"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'%s','%s', %s) ",
							thissheep_id, 0, 0, 0, 0, 0, 0,
							0, 0, 0, 0, 0, 0,
							0, 0, 0, 0, 0, 0,
							0, 0, 16, trait11_data, 0, 0,
							0, 0, 0, 0, 0, 0,
							0, 0, 0, 0, 0, 0,
							0, 0, 0, 0,
							1, 0, 0, 0, 0, mytoday, mytime, temp_integer);
					Log.i("add evaluation ", "cmd is " + cmd);
					dbh.exec(cmd);
					Log.i("add evaluation ", "after insert into sheep_evaluation_table");
				}
				}
		}
			clearBtn( null );
	}
	public void printLabel( View v ){
		try
		{
			String[] lines = EID.split("\n"); // works for both
			String contents = LastEID.substring(0, 3) + LastEID.substring(4, 16);
			Log.i("PrintLabel btn ", " contents " + contents);
			Intent encodeIntent = new Intent("weyr.LT.ENCODE");
			encodeIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			encodeIntent.addCategory(Intent.CATEGORY_DEFAULT);
			encodeIntent.putExtra("ENCODE_FORMAT", "CODE_128");
			encodeIntent.putExtra("ENCODE_SHOW_CONTENTS", false);
			encodeIntent.putExtra("ENCODE_DATA", contents);
			encodeIntent.putExtra("ENCODE_AUTOPRINT", "false");

			if (AutoPrint) {
				encodeIntent.putExtra("ENCODE_AUTOPRINT", "true");
				Log.i("PrintLabel btn ", " autoprint is true ");
			};

			encodeIntent.putExtra("ENCODE_DATA1", LabelText);
			encodeIntent.putExtra("ENCODE_DATE", Utilities.TodayIs() + "  " + Utilities.TimeIs());
			Log.i("PrintLabel btn ", " before put extra sheepName ");
			encodeIntent.putExtra("ENCODE_SHEEPNAME", SheepName);
			Log.i("PrintLabel btn ", " after put extra sheepName " + SheepName);
			startActivity(encodeIntent);
			Log.i("PrintLabel btn ", " after start activity encode " );
		}
		catch(Exception r)
		{
			Log.v("PrintLabel ", " in sheep management RunTimeException: " + r);
		}
	}

	public void backBtn( View v )
	{
		doUnbindService();
		doUnbindScaleService();
		doUnbindbaacodeService();
		stopService(new Intent(TissueSample.this, eidService.class));
		stopService(new Intent(TissueSample.this, scaleService.class));
		stopService(new Intent(TissueSample.this, baacodeService.class));
		// Added this to close the database if we go back to the main activity
		try {
			cursor.close();
		}catch (Exception r)
		{
			Log.i("back btn", "cursor RunTimeException: " + r);
		}
		try {
			cursor2.close();
		}catch (Exception r)
		{
			Log.i("back btn", "cursor2 RunTimeException: " + r);
		}
		try {
			cursor3.close();
		}catch (Exception r)
		{
			Log.i("back btn", " cursor3 RunTimeException: " + r);
		}
		dbh.closeDB();
		clearBtn( null );
		//Go back to main
		this.finish();
	}

	public void showAlert(View v)
	{
		String	alert_text;
		String 			dbname = getString(R.string.real_database_file);
		String          cmd;
		Object 			crsr;
		// Display alerts here
		AlertDialog.Builder builder = new AlertDialog.Builder( this );
		cmd = String.format("select sheep_table.alert01 from sheep_table where sheep_id =%d", thissheep_id);
		Log.i("evalGetAlert ", cmd);
		crsr = dbh.exec( cmd );
		cursor   = ( Cursor ) crsr;
		dbh.moveToFirstRecord();
		alert_text = (dbh.getStr(0));
		Log.i("evalShowAlert ", alert_text);
		builder.setMessage( alert_text )
				.setTitle( R.string.alert_warning );
		builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int idx) {
				// User clicked OK button
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	public void clearBtn( View v )
	{
		TextView TV ;
		TV = (TextView) findViewById( R.id.inputText );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.sheepnameText );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.sireName );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.damName );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.sample_container_id );
		TV.setText( "" );
		//	Need to clear out the rest of the tags here
		Log.i("clear btn", "before changing myadapter");
		try {
			myadapter.changeCursor(null);
		}
		catch (Exception e) {
			// In this case there is no adapter so do nothing
		}
		Log.i("clear btn", "after changing myadapter");

		// Enable Update Database button and make it normal
		btn = (Button) findViewById( R.id.update_database_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFFFFFFFF, 0xFF000000));
		btn.setEnabled(true);

		// make the alert button normal and disabled
		btn = (Button) findViewById( R.id.alert_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
		btn.setEnabled(false);
	}

	@Override
	public void onResume (){
		super.onResume();
		CheckIfServiceIsRunning();
		Log.i("TissueSample", " OnResume");
//		scanEid( null );
//		Log.i("TissueSample", " OnResume after scanEID(null)");
	}

	@Override
	public void onPause (){
		super.onPause();
		Log.i("TissueSample", " OnPause");
		doUnbindService();
		doUnbindScaleService();
		doUnbindbaacodeService();
	}

	public void helpBtn( View v )
	{
		// Display help here
		AlertDialog.Builder builder = new AlertDialog.Builder( this );
		builder.setMessage( R.string.help_management )
				.setTitle( R.string.help_warning );
		builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int idx) {
				// User clicked OK button

			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	public boolean tableExists (String table){
		try {
			dbh.exec("select * from "+ table);
			return true;
		} catch (SQLiteException e) {
			return false;
		}
	}

	public void doNote( View v )
	{
		Utilities.takeNote(v, thissheep_id, this);
	}


	//  user clicked 'Scan' button
	public void scanEid( View v){
		// Here is where I need to get a tag scanned and put the data into the variable LastEID
		clearBtn( v );
		tag_type_spinner.setSelection(1);
//		Log.i("in ScanEID", " after set tag_type_spinner ");
		if (mService != null) {
			try {
				//Start eidService sending tags
				Message msg = Message.obtain(null, eidService.MSG_SEND_ME_TAGS);
				msg.replyTo = mMessenger;
				mService.send(msg);
				//	make the scan eid button  0x0000FF00, 0xff00ff00
				Button btn = (Button) findViewById(R.id.scan_eid_btn);
				btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}
	}
	public void scanWeight(View v) {

		if (mScaleService != null) {
			try {
				//Start scaleService sending tags
				Message msg = Message.obtain(null, scaleService.MSG_SEND_ME_TAGS);
				msg.replyTo = mMessenger;
				mScaleService.send(msg);
				// Here is where to get the scanned weight
				//	make the scan weight button  0x0000FF00, 0xff00ff00
				Button btn = (Button) findViewById(R.id.scan_weight_btn);
				btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));
				boxweight = (CheckBox) findViewById(R.id.checkBoxTakeWeight);
				boxweight.setChecked(true);
			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		} else {
//					 Log.i("in ScanWeight", " ready for data " );
		}
	}

	public void scanTSU( View v){

		if (mBaaCodeService != null) {
			try {
				//Start baacodeService sending tags
				Message msg = Message.obtain(null, baacodeService.MSG_SEND_ME_TAGS);
				Log.i("Tissue", " after baacode send me tags msg sent ");
				msg.replyTo = mMessenger;
				Log.i("Tissue", " after msg.reply in scanTSU part ");
				mBaaCodeService.send(msg);
				//	make the scan baacode button  0x0000FF00, 0xff00ff00
				Button btn = (Button) findViewById(R.id.scan_sample_container_btn);
				btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));
				 Log.i("Tissue", " set to scan TSU ");
			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}
	}

		// user clicked the "next record" button
	public void nextRecord( View v)
	{
		//	Clear out the display first
		clearBtn( v );
		//	Go get the sheep id of this record
		Log.i("in next record", "this sheep ID is " + String.valueOf(thissheep_id));
		sheepcursor.moveToNext();
		Log.i("in next record", "after moving the cursor ");
		thissheep_id = sheepcursor.getInt(0);
		Log.i("in next record", "this sheep ID is " + String.valueOf(thissheep_id));
		recNo         += 1;
		formatSheepRecord(v);
//		    		// I've moved forward so I need to enable the previous record button
		Button btn3 = (Button) findViewById( R.id.prev_rec_btn );
		btn3.setEnabled(true);
		if (recNo == (nRecs)) {
			// at end so disable next record button
			Button btn2 = (Button) findViewById( R.id.next_rec_btn );
			btn2.setEnabled(false);
		}
	}

	// user clicked the "previous record" button
	public void prevRecord( View v){
//			    	Clear out the display first
		clearBtn( v );
		Log.i("in prev record", "this sheep ID is " + String.valueOf(thissheep_id));
		sheepcursor.moveToPrevious();
		Log.i("in prev record", "after moving the cursor5 ");
		thissheep_id = sheepcursor.getInt(0);
		Log.i("in prev record", "this sheep ID is " + String.valueOf(thissheep_id));
		recNo         -= 1;
		formatSheepRecord(v);
		// I've moved back so enable the next record button
		Button btn2 = (Button) findViewById( R.id.next_rec_btn );
		btn2.setEnabled(true);
		if (recNo == 1) {
			// at beginning so disable prev record button
			Button btn3 = (Button) findViewById( R.id.prev_rec_btn );
			btn3.setEnabled(false);
		}
	}
}
