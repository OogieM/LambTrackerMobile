package com.weyr_associates.lambtracker;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import java.util.ArrayList;
import java.util.List;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Toast;
import android.util.Log;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.content.Context;
import android.view.LayoutInflater;

import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class TestInterfaceDesigns extends Activity
{
	private DatabaseHandler dbh;
	int             id;
	int             fedtagid, farmtagid, eidtagid;
	public String 	 eidText, alert_text;

	public int 		thissheep_id, thissire_id, thisdam_id;
	public String 	thissheep_name, thissire_name, thisdam_name;

	// Sire side pedigree holders
	public int   p2ss_id, p2sd_id, p3sss_id, p3ssd_id, p3sds_id, p3sdd_id ;
	public int   p4ssss_id, p4sssd_id, p4ssds_id, p4ssdd_id, p4sdss_id, p4sdsd_id, p4sdds_id, p4sddd_id ;
	public String   p2ss_name, p2sd_name, p3sss_name, p3ssd_name, p3sds_name, p3sdd_name ;
	public String   p4ssss_name, p4sssd_name, p4ssds_name, p4ssdd_name, p4sdss_name, p4sdsd_name, p4sdds_name, p4sddd_name ;

	// Dam side pedigree holders
	public int   p2ds_id, p2dd_id, p3dss_id, p3dsd_id, p3dds_id, p3ddd_id ;
	public int   p4dsss_id, p4dssd_id, p4dsds_id, p4dsdd_id, p4ddss_id, p4ddsd_id, p4ddds_id, p4dddd_id ;
	public String   p2ds_name, p2dd_name, p3dss_name, p3dsd_name, p3dds_name, p3ddd_name ;
	public String   p4dsss_name, p4dssd_name, p4dsds_name, p4dsdd_name, p4ddss_name, p4ddsd_name, p4ddds_name, p4dddd_name ;

	public Cursor 	cursor, cursor2, cursor3, cursor4, cursor5, sheepcursor, drugCursor, tagcursor, ownercursor, locationcursor;
	public Object	crsr;
	public Spinner tag_type_spinner, tag_location_spinner, tag_color_spinner ;
	public List<String> tag_types, tag_locations, tag_colors;
	public List<String> tag_types_display_order, tag_types_id_order;
	public Spinner predefined_note_spinner01, predefined_note_spinner02, predefined_note_spinner03;
	public Spinner predefined_note_spinner04, predefined_note_spinner05;

	public int             nRecs;
	private int			    recNo;

	ArrayAdapter<String> dataAdapter;
	String     	cmd ;
	Integer 	i;
	public Button btn , btn2;

	public SimpleCursorAdapter myadapter, myadapter2, drugAdapter;

	/////////////////////////////////////////////////////
	Messenger mService = null;
	boolean mIsBound;

	final Messenger mMessenger = new Messenger(new TestInterfaceDesigns.IncomingHandler());
	// variable to hold the string
	public String LastEID ;

	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case eidService.MSG_UPDATE_STATUS:
					Bundle b1 = msg.getData();

					break;
				case eidService.MSG_NEW_EID_FOUND:
					Bundle b2 = msg.getData();

					LastEID = (b2.getString("info1"));
					//We have a good whole EID number
					Log.i ("in handler case" , "got eid of " + LastEID);
					gotEID ();
					break;
				case eidService.MSG_UPDATE_LOG_APPEND:
					//Bundle b3 = msg.getData();
					//Log.i("Convert", "Add to Log.");

					break;
				case eidService.MSG_UPDATE_LOG_FULL:
					//Log.i("Convert", "Log Full.");

					break;
				case eidService.MSG_THREAD_SUICIDE:
					Log.i("Convert", "Service informed Activity of Suicide.");
					doUnbindService();
					stopService(new Intent(TestInterfaceDesigns.this, eidService.class));

					break;
				default:
					super.handleMessage(msg);
			}
		}
	}

	public ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			mService = new Messenger(service);
			Log.i("Convert", "At Service.");
			try {
				//Register client with service
				Message msg = Message.obtain(null, eidService.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);

				//Request a status update.
				//msg = Message.obtain(null, eidService.MSG_UPDATE_STATUS, 0, 0);
				//				mService.send(msg);

				//Request full log from service.
				//				msg = Message.obtain(null, eidService.MSG_UPDATE_LOG_FULL, 0, 0);
				//				mService.send(msg);

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}
		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been unexpectedly disconnected - process crashed.
			mService = null;
		}
	};

	private void CheckIfServiceIsRunning() {
		//If the service is running when the activity starts, we want to automatically bind to it.
		Log.i("Convert", "At isRunning?.");
		if (eidService.isRunning()) {
			//Log.i("Convert", "is.");
			doBindService();
		} else {
			//Log.i("Convert", "is not, start it");
			startService(new Intent(TestInterfaceDesigns.this, eidService.class));
			doBindService();
		}
		//Log.i("Convert", "Done isRunning.");
	}

	void doBindService() {
		// Establish a connection with the service.  We use an explicit
		// class name because there is no reason to be able to let other
		// applications replace our component.
		//Log.i("Convert", "At doBind1.");
		bindService(new Intent(this, eidService.class), mConnection, Context.BIND_AUTO_CREATE);
		//Log.i("Convert", "At doBind2.");

		mIsBound = true;

		if (mService != null) {
			//Log.i("Convert", "At doBind3.");
			try {
				//Request status update
				Message msg = Message.obtain(null, eidService.MSG_UPDATE_STATUS, 0, 0);
				msg.replyTo = mMessenger;
				mService.send(msg);
				Log.i("Convert", "At doBind4.");
				//Request full log from service.
				msg = Message.obtain(null, eidService.MSG_UPDATE_LOG_FULL, 0, 0);
				mService.send(msg);
			} catch (RemoteException e) {}
		}
		//Log.i("Convert", "At doBind5.");
	}
	void doUnbindService() {
		//Log.i("Convert", "At DoUnbindservice");
		if (mService != null) {
			try {
				//Stop eidService from sending tags
				Message msg = Message.obtain(null, eidService.MSG_NO_TAGS_PLEASE);
				msg.replyTo = mMessenger;
				mService.send(msg);

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}
		if (mIsBound) {
			// If we have received the service, and hence registered with it, then now is the time to unregister.
			if (mService != null) {
				try {
					Message msg = Message.obtain(null, eidService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = mMessenger;
					mService.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service has crashed.
				}
			}
			// Detach our existing connection.
			unbindService(mConnection);
			mIsBound = false;
		}
	}

	public void gotEID( )
	{
		//	make the scan eid button red
		btn = (Button) findViewById( R.id.scan_eid_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
		// 	Display the EID number
		TextView TV = (TextView) findViewById (R.id.inputText);
		TV.setText( LastEID );
		Log.i("in gotEID ", "with LastEID of " + LastEID);
		btn2 = (Button) findViewById (R.id.look_up_sheep_btn);
		btn2.performClick();
	}
	/////////////////////////////////////////////////////
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test_interface_designs);
		Log.i("LookUpSheep", " after set content view");
		View v = null;
		String 	dbfile = getString(R.string.real_database_file) ;
		Log.i("LookUpSheep", " after get database file");
		dbh = new DatabaseHandler( this, dbfile );
		Object crsr;

//		Added the variable definitions here
		String          cmd;


		////////////////////////////////////
		CheckIfServiceIsRunning();
		Log.i("LookUpSheep", "back from isRunning");
		////////////////////////////////////
		p2ss_id = 0;
		p2sd_id = 0;
		p3sss_id = 0;
		p3ssd_id = 0;
		p3sds_id = 0;
		p3sdd_id = 0;
		p4ssss_id = 0;
		p4sssd_id = 0;
		p4ssds_id = 0;
		p4ssdd_id = 0;
		p4sdss_id = 0;
		p4sdsd_id = 0;
		p4sdds_id = 0;
		p4sddd_id = 0;

		p2ds_id = 0;
		p2dd_id = 0;
		p3dss_id = 0;
		p3dsd_id = 0;
		p3dds_id = 0;
		p3ddd_id = 0;
		p4dsss_id = 0;
		p4dssd_id = 0;
		p4dsds_id = 0;
		p4dsdd_id = 0;
		p4ddss_id = 0;
		p4ddsd_id = 0;
		p4ddds_id = 0;
		p4dddd_id = 0;

		thissheep_id = 0;
		thissire_id = 0;
		thisdam_id = 0;

		// Fill the Tag Type Spinner
		tag_type_spinner = (Spinner) findViewById(R.id.tag_type_spinner);
		tag_types = new ArrayList<String>();
		tag_types_id_order= new ArrayList<String>();
		tag_types_display_order = new ArrayList<String>();
		// Select All fields from id types to build the spinner
		cmd = "select * from id_type_table order by id_type_display_order";
		Log.i("fill tag spinner", "command is " + cmd);
		crsr = dbh.exec( cmd );
		cursor5   = ( Cursor ) crsr;
		dbh.moveToFirstRecord();
		tag_types.add("Select a Type");
		tag_types_id_order.add("tag_types_id");
		tag_types_display_order.add("tag_type_display_order");
		Log.i("fill tag spinner", "added first option ");
		// looping through all rows and adding to list
		for (cursor5.moveToFirst(); !cursor5.isAfterLast(); cursor5.moveToNext()){
			tag_types_id_order.add(cursor5.getString(0));
			Log.i("LookUpSheep", "tag_types_id_order " + cursor5.getString(0));
			tag_types.add(cursor5.getString(1));
			Log.i("LookUpSheep", "tag_type name" + cursor5.getString(1));
			tag_types_display_order.add(cursor5.getString(3));
			Log.i("LookUpSheep", "tag_types_display_order " + cursor5.getString(3));
		}

		// Creating adapter for spinner
		dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, tag_types);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		tag_type_spinner.setAdapter (dataAdapter);
		//	set initial tag type to look for to be name it's based on the display order
		// TODO: 2/25/19
		tag_type_spinner.setSelection(4);

		// make the alert button normal and disabled
		btn = (Button) findViewById( R.id.alert_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
		btn.setEnabled(false);
		//	Disable the Next Record and Prev. Record button until we have multiple records
		btn = (Button) findViewById( R.id.next_rec_btn );
		btn.setEnabled(false);
		btn = (Button) findViewById( R.id.prev_rec_btn );
		btn.setEnabled(false);

		//	make the scan eid button red
		btn = (Button) findViewById( R.id.scan_eid_btn );
		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));

	}
	public void lookForSheep (View v){

		Object crsr;
		Boolean exists;
		Integer temp1;
		String temp2;
		TextView TV;
		ListView notelist = (ListView) findViewById(R.id.list2);

		exists = true;
		// Hide the keyboard when you click the button
		InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
		//	Disable the Next Record and Prev. Record button until we have multiple records
		btn = (Button) findViewById( R.id.next_rec_btn );
		btn.setEnabled(false);
		btn = (Button) findViewById( R.id.prev_rec_btn );
		btn.setEnabled(false);

		TV = (EditText) findViewById( R.id.inputText );
		String	tag_num = TV.getText().toString();

		Log.i("LookForSheep", " got to lookForSheep with Tag Number of " + String.valueOf(tag_num));
		Log.i("LookForSheep", " got to lookForSheep with Tag position of " + tag_type_spinner.getSelectedItemPosition());
		temp2 = String.valueOf(tag_type_spinner.getSelectedItemPosition());
		Log.i("LookForSheep", " got to lookForSheep with Tag spinner number of " + temp2);
		temp1 = Integer.valueOf(temp2);
		temp2 = tag_types_id_order.get(temp1);
		Log.i("LookForSheep", " got to lookForSheep with Tag type id order number of " + temp2);

		temp1 = Integer.valueOf(temp2);
		Log.i("LookForSheep", " got to lookForSheep with Tag type id number of " + String.valueOf(temp1));

		exists = tableExists("sheep_table");
		if (exists){
			//			switch (tag_type_spinner.getSelectedItemPosition()){
			switch (temp1){
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					if( tag_num != null && tag_num.length() > 0 ){
						// Get the sheep id from the id table for this tag number and selected tag type
//    						cmd = String.format( "select sheep_id from id_info_table where tag_number='%s' "+
//				        			"and id_info_table.tag_type='%s' and (id_info_table.tag_date_off is null or" +
//				        			" id_info_table.tag_date_off = '') "
//				        			, tag_num , tag_type_spinner.getSelectedItemPosition());
						cmd = String.format( "select sheep_id from id_info_table where tag_number='%s' "+
										"and id_info_table.tag_type='%s' "
								, tag_num , temp1);
						Log.i("searchByNumber", "command is " + cmd);
						crsr = dbh.exec( cmd );
						cursor   = ( Cursor ) crsr;
						recNo    = 1;
						nRecs    = cursor.getCount();
						Log.i("searchByNumber", " nRecs = "+ String.valueOf(nRecs));
						dbh.moveToFirstRecord();
						Log.i("searchByNumber", " the cursor is of size " + String.valueOf(dbh.getSize()));
						if( dbh.getSize() == 0 ){
							// no sheep with that  tag in the database so clear out and return
							clearBtn( v );
							TV = (TextView) findViewById( R.id.sheepnameText );
							TV.setText( "Cannot find this sheep." );
							return;
						}
						thissheep_id = dbh.getInt(0);
						Log.i("searchByNumber", "This sheep is record " + String.valueOf(thissheep_id));
						if (nRecs >1){
							//	Have multiple sheep with this tag so enable next button
							btn = (Button) findViewById( R.id.next_rec_btn );
							btn.setEnabled(true);
						}
						Log.i("searchByNumber", " Before formatting the record");
						//	We need to call the format the record method
						formatSheepRecord(v);
					}
					break;
				case 6:
					//	got a split
					//	Assume no split ears at this time.
					//	Needs modification for future use
//				    	TV = (TextView) findViewById( R.id.sheepnameText );
//			        	TV.setText( "Cannot search on splits yet." );
//				    	// TODO
//				        break;
					if( tag_num != null && tag_num.length() < 1 ){
						// Get the sheep id from the id table for split ears
						cmd = String.format( "select sheep_id from id_info_table where tag_number='%s' "+
										"and id_info_table.tag_type='%s' "
								, tag_num , temp1);
						Log.i("searchBysplit", "command is " + cmd);
						crsr = dbh.exec( cmd );
						cursor   = ( Cursor ) crsr;
						recNo    = 1;
						nRecs    = cursor.getCount();
						Log.i("searchByNumber", " nRecs = "+ String.valueOf(nRecs));
						dbh.moveToFirstRecord();
						Log.i("searchByNumber", " the cursor is of size " + String.valueOf(dbh.getSize()));
						if( dbh.getSize() == 0 ){
							// no sheep with that  tag in the database so clear out and return
							clearBtn( v );
							TV = (TextView) findViewById( R.id.sheepnameText );
							TV.setText( "Cannot find this sheep." );
							return;
						}
						thissheep_id = dbh.getInt(0);
						Log.i("searchByNumber", "This sheep is record " + String.valueOf(thissheep_id));
						if (nRecs >1){
							//	Have multiple sheep with this tag so enable next button
							btn = (Button) findViewById( R.id.next_rec_btn );
							btn.setEnabled(true);
						}
						Log.i("searchByNumber", " Before formatting the record");
						//	We need to call the format the record method
						formatSheepRecord(v);
					}
					break;
				case 7:
					//	got a notch
					//	Assume no notches at this time.
					//	Needs modification for future use
//				    	TV = (TextView) findViewById( R.id.sheepnameText );
//			        	TV.setText( "Cannot search on notches yet." );
//				    	// TODO
//				        break;
					if( tag_num != null && tag_num.length() < 1 ){
						// Get the sheep id from the id table for split ears
						cmd = String.format( "select sheep_id from id_info_table where tag_number='%s' "+
										"and id_info_table.tag_type='%s' "
								, tag_num , temp1);
						Log.i("searchBysplit", "command is " + cmd);
						crsr = dbh.exec( cmd );
						cursor   = ( Cursor ) crsr;
						recNo    = 1;
						nRecs    = cursor.getCount();
						Log.i("searchByNumber", " nRecs = "+ String.valueOf(nRecs));
						dbh.moveToFirstRecord();
						Log.i("searchByNumber", " the cursor is of size " + String.valueOf(dbh.getSize()));
						if( dbh.getSize() == 0 ){
							// no sheep with that  tag in the database so clear out and return
							clearBtn( v );
							TV = (TextView) findViewById( R.id.sheepnameText );
							TV.setText( "Cannot find this sheep." );
							return;
						}
						thissheep_id = dbh.getInt(0);
						Log.i("searchByNumber", "This sheep is record " + String.valueOf(thissheep_id));
						if (nRecs >1){
							//	Have multiple sheep with this tag so enable next button
							btn = (Button) findViewById( R.id.next_rec_btn );
							btn.setEnabled(true);
						}
						Log.i("searchByNumber", " Before formatting the record");
						//	We need to call the format the record method
						formatSheepRecord(v);
					}
					break;
				case 8:
					//	got a name
					// TODO
					tag_num = "%" + tag_num + "%";
					// Modified this so I can look up removed sheep as well
//			        	cmd = String.format( "select sheep_id, sheep_name from sheep_table where sheep_name like '%s'" +
//			        			" and (remove_date is null or remove_date = '') "
//			        			, tag_num );
					cmd = String.format( "select sheep_id, sheep_name from sheep_table where sheep_name like '%s'"
							, tag_num );
					Log.i("searchByName", "command is " + cmd);
					crsr = dbh.exec( cmd );
					cursor   = ( Cursor ) crsr;
					recNo    = 1;
					nRecs    = cursor.getCount();
					Log.i("searchByName", " nRecs = "+ String.valueOf(nRecs));
					dbh.moveToFirstRecord();
					Log.i("searchByName", " the cursor is of size " + String.valueOf(dbh.getSize()));
					if( dbh.getSize() == 0 )
					{ // no sheep with that name in the database so clear out and return
						clearBtn( v );
						TV = (TextView) findViewById( R.id.sheepnameText );
						TV.setText( "Cannot find this sheep." );
						return;
					}
					thissheep_id = dbh.getInt(0);
					if (nRecs >1){
						//	Have multiple sheep with this name so enable next button
						btn = (Button) findViewById( R.id.next_rec_btn );
						btn.setEnabled(true);
					}
					//	We need to call the format the record method
					formatSheepRecord(v);
					break;
			} // end of case switch
		}else {
			clearBtn( null );
			TV = (TextView) findViewById( R.id.sheepnameText );
			TV.setText( "Sheep Database does not exist." );
		}
	}

	public void formatSheepRecord (View v){
		Object crsr, crsr2, crsr3, crsr4, drugCrsr;
		TextView TV;
		int last_owner;
		ListView notelist = (ListView) findViewById(R.id.list2);
		ListView drugList = (ListView) findViewById(R.id.druglist);

		thissheep_id = cursor.getInt(0);
		Log.i("format record", "This sheep is record " + String.valueOf(thissheep_id));

		cmd = String.format( "select sheep_table.sheep_name, sheep_table.sheep_id,  " +
				" sheep_table.alert01,  " +
				"sheep_table.sire_id, sheep_table.dam_id, sheep_table.birth_date, birth_type_table.birth_type, " +
				"sex_table.sex_name, sheep_table.birth_weight, sheep_table.death_date, death_reason_table.death_reason " +
				", cluster_table.cluster_name  " +
				", codon171_table.codon171_alleles, flock_prefix_table.flock_name  " +
				"from sheep_table  " +
				"left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
				"inner join birth_type_table on id_birthtypeid = sheep_table.birth_type " +
				"left outer join codon171_table on sheep_table.codon171 = codon171_table.id_codon171id " +
				"inner join sex_table on sex_table.sex_sheepid = sheep_table.sex " +
				"left outer join death_reason_table on sheep_table.death_reason = death_reason_table.id_deathreasonid " +
				"left outer join sheep_cluster_table on sheep_table.sheep_id = sheep_cluster_table.sheep_id " +
				"left join cluster_table on cluster_table.id_clusternameid = sheep_cluster_table.id_clusterid " +
				"where sheep_table.sheep_id ='%s' " +
				" ", thissheep_id);

		Log.i("format record", " comand is " + cmd);
		crsr = dbh.exec( cmd );
		Log.i("format record", " after run the command");
		sheepcursor   = ( Cursor ) crsr;
		sheepcursor.moveToFirst();
		Log.i("format record", " the cursor is of size " + String.valueOf(dbh.getSize()));
		thissheep_name = dbh.getStr(13) + " " +dbh.getStr(0);
		TV = (TextView) findViewById( R.id.sheepnameText );
//		TV.setText (dbh.getStr(0));
		TV.setText (thissheep_name );

		Log.i("format record", "after get sheep name ");
		alert_text = dbh.getStr(2);
		Log.i("format record", "after get alert ");
		TV = (TextView) findViewById( R.id.birth_date );
		TV.setText (dbh.getStr(5));
		Log.i("format record", "after get birth date ");
		TV = (TextView) findViewById( R.id.birth_type );
		TV.setText (dbh.getStr(6));
		Log.i("format record", "after get birth type ");
		TV = (TextView) findViewById( R.id.sheep_sex );
		TV.setText (dbh.getStr(7));
		Log.i("format record", "after get sheep sex ");
		TV = (TextView) findViewById( R.id.birth_weight );
		TV.setText (String.valueOf(dbh.getReal(8)));
		Log.i("format record", "after get birth weight ");
		TV = (TextView) findViewById( R.id.death_date );
		TV.setText (dbh.getStr(9));
		Log.i("format record", "after get death date ");
		TV = (TextView) findViewById( R.id.death_reason );
		TV.setText (dbh.getStr(10));
		Log.i("format record", "after get death reason ");
		TV = (TextView) findViewById( R.id.cluster_name );
		TV.setText (dbh.getStr(11));
		Log.i("format record", "after get cluster name ");
		TV = (TextView) findViewById( R.id.codon );
		TV.setText (dbh.getStr(12));
		Log.i("format record", "after get codon 171 ");


		//	Get the sire and dam id numbers
		thissire_id = dbh.getInt(3);
		Log.i("format record", " Sire is " + String.valueOf(thissire_id));
		thisdam_id = dbh.getInt(4);
		Log.i("format record", " Dam is " + String.valueOf(thisdam_id));

//	Log.i("format record", "before transfers ");
		cmd = String.format( "select * FROM sheep_ownership_history_table WHERE sheep_id ='%s' " +
				" ORDER BY transfer_date ", thissheep_id);
		Log.i("format record", " comand is " + cmd);
		crsr = dbh.exec( cmd );
		Log.i("format record", " after run the command");
		ownercursor   = ( Cursor ) crsr;
		ownercursor.moveToLast();
		Log.i("format record", " the cursor is of size " + String.valueOf(dbh.getSize()));

		TV = (TextView) findViewById(R.id.transfer_date);
		TV.setText(dbh.getStr(2));
		last_owner = dbh.getInt(4);

		cmd = String.format( "select contact_first_name, contact_Last_name FROM contacts_table WHERE id_contactsid ='%s' " +
				" ", last_owner);
		Log.i("format record", " comand is " + cmd);
		crsr = dbh.exec( cmd );
		Log.i("format record", " after run the command");
		ownercursor   = ( Cursor ) crsr;
		ownercursor.moveToFirst();
		TV = (TextView) findViewById( R.id.owner );
		TV.setText (dbh.getStr(0)+ " " + dbh.getStr(1));
		Log.i("format record", " owner is " + dbh.getStr(0)+ dbh.getStr(1));
		Log.i("format record", "after get owner ");

		//	Go get the sire name
//		if (thissire_id != 0){
//			cmd = String.format( "select sheep_table.sheep_name from sheep_table where sheep_table.sheep_id = '%s'", thissire_id);
//			Log.i("format record", " cmd is " + cmd);
//			crsr2 = dbh.exec( cmd);
//			Log.i("format record", " after second db lookup");
//			cursor2   = ( Cursor ) crsr2;
//			cursor2.moveToFirst();
//			TV = (TextView) findViewById( R.id.sireName );
//			thissire_name = dbh.getStr(0);
//			TV.setText (thissire_name);
//			Log.i("format record", " Sire is " + thissire_name);
//			Log.i("format record", " Sire is " + String.valueOf(thissire_id));
//		}
		//	Go get the sire pedigree
		if (thissire_id != 0){
			cmd = String.format( "select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
			" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
			" where sheep_table.sheep_id = '%s'", thissire_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec( cmd);
			Log.i("format record", " after first db lookup");
			cursor2   = ( Cursor ) crsr2;
			cursor2.moveToFirst();
			thissire_name = ("     " + dbh.getStr(0)+ " " + dbh.getStr(1));
			p2ss_id = dbh.getInt(2);
			p2sd_id = dbh.getInt(3);
			TV = (TextView) findViewById( R.id.sireName );
			TV.setText (thissire_name);
			Log.i("format record", " Sire is " + thissire_name);
			Log.i("format record", " Sire is " + String.valueOf(thissire_id));
		}

		if (p2ss_id != 0){
			cmd = String.format( "select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p2ss_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec( cmd);
			Log.i("format record", " after second db lookup");
			cursor2   = ( Cursor ) crsr2;
			cursor2.moveToFirst();
			p2ss_name = ("          " + dbh.getStr(0)+ " " + dbh.getStr(1));
			p3sss_id = dbh.getInt(2);
			p3ssd_id = dbh.getInt(3);
			TV = (TextView) findViewById( R.id.p2ssName );
			TV.setText (p2ss_name);
			Log.i("format record", " Sire is " + p2ss_name);
			Log.i("format record", " Sire is " + String.valueOf(p2ss_id));
		}

		if (p2sd_id != 0){
			cmd = String.format( "select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p2sd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec( cmd);
			Log.i("format record", " after third db lookup");
			cursor2   = ( Cursor ) crsr2;
			cursor2.moveToFirst();
			p2sd_name = ("          " + dbh.getStr(0)+ " " + dbh.getStr(1));
			p3sds_id = dbh.getInt(2);
			p3sdd_id = dbh.getInt(3);
			TV = (TextView) findViewById( R.id.p2sdName );
			TV.setText (p2sd_name);
			Log.i("format record", " Dam is " + p2sd_name);
			Log.i("format record", " Dam is " + String.valueOf(p2sd_id));
		}

		if (p3sss_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p3sss_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after fourth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p3sss_name = ("               " + dbh.getStr(0) + " " + dbh.getStr(1));
			p4ssss_id = dbh.getInt(2);
			p4sssd_id = dbh.getInt(3);
			TV = (TextView) findViewById(R.id.p3sssName);
			TV.setText(p3sss_name);
			Log.i("format record", " Sire is " + p3sss_name);
			Log.i("format record", " Sire is " + String.valueOf(p3sss_id));
		}

		if (p3ssd_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
						" where sheep_table.sheep_id = '%s'", p3ssd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after fifth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p3ssd_name = ("               " + dbh.getStr(0) + " " + dbh.getStr(1));
			p4ssds_id = dbh.getInt(2);
			p4ssdd_id = dbh.getInt(3);
			TV = (TextView) findViewById(R.id.p3ssdName);
			TV.setText(p3ssd_name);
			Log.i("format record", " Dam is " + p3ssd_name);
			Log.i("format record", " Dam is " + String.valueOf(p3ssd_id));
		}

		if (p3sds_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p3sds_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after sixth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p3sds_name = ("               " + dbh.getStr(0) + " " + dbh.getStr(1));
			p4sdss_id = dbh.getInt(2);
			p4sdsd_id = dbh.getInt(3);
			TV = (TextView) findViewById(R.id.p3sdsName);
			TV.setText(p3sds_name);
			Log.i("format record", " Sire is " + p3sds_name);
			Log.i("format record", " Sire is " + String.valueOf(p3sds_id));
		}

		if (p3sdd_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p3sdd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after seventh db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p3sdd_name = ("               " + dbh.getStr(0) + " " + dbh.getStr(1));
			p4sdds_id = dbh.getInt(2);
			p4sddd_id = dbh.getInt(3);
			TV = (TextView) findViewById(R.id.p3sddName);
			TV.setText(p3sdd_name);
			Log.i("format record", " Dam is " + p3sdd_name);
			Log.i("format record", " Dam is " + String.valueOf(p3sdd_id));
		}

		if (p4ssss_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4ssss_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after eighth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4ssss_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4ssssName);
			TV.setText(p4ssss_name);
			Log.i("format record", " Sire is " + p4ssss_name);
			Log.i("format record", " Sire is " + String.valueOf(p4ssss_id));
		}

		if (p4sssd_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4sssd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after ninth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4sssd_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4sssdName);
			TV.setText(p4sssd_name);
			Log.i("format record", " Dam is " + p4sssd_name);
			Log.i("format record", " Dam is " + String.valueOf(p4sssd_id));
		}

		if (p4ssds_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4ssds_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after tenth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4ssds_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4ssdsName);
			TV.setText(p4ssds_name);
			Log.i("format record", " Sire is " + p4ssds_name);
			Log.i("format record", " Sire is " + String.valueOf(p4ssds_id));
		}

		if (p4ssdd_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4ssdd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after eleventh db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4ssdd_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4ssddName);
			TV.setText(p4ssdd_name);
			Log.i("format record", " Dam is " + p4ssdd_name);
			Log.i("format record", " Dam is " + String.valueOf(p4ssdd_id));
		}

		if (p4sdss_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4sdss_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after twelfth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4sdss_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4sdssName);
			TV.setText(p4sdss_name);
			Log.i("format record", " Sire is " + p4sdss_name);
			Log.i("format record", " Sire is " + String.valueOf(p4sdss_id));
		}

		if (p4sdsd_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4sdsd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after thirteenth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4sdsd_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4sdsdName);
			TV.setText(p4sdsd_name);
			Log.i("format record", " Dam is " + p4sdsd_name);
			Log.i("format record", " Dam is " + String.valueOf(p4sdsd_id));
		}

		if (p4sdds_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4sdds_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after fourteenth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4sdds_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4sddsName);
			TV.setText(p4sdds_name);
			Log.i("format record", " Sire is " + p4sdds_name);
			Log.i("format record", " Sire is " + String.valueOf(p4sdds_id));
		}

		if (p4sddd_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4sddd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after fifteenth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4sddd_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4sdddName);
			TV.setText(p4sddd_name);
			Log.i("format record", " Dam is " + p4sddd_name);
			Log.i("format record", " Dam is " + String.valueOf(p4sddd_id));
		}

		//	Go get the dam pedigree
		if(thisdam_id != 0){
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", thisdam_id);
			crsr3 = dbh.exec( cmd);
			cursor3   = ( Cursor ) crsr3;
			cursor3.moveToFirst();
			TV = (TextView) findViewById( R.id.damName );
			thisdam_name = ("     " + dbh.getStr(0)+ " " + dbh.getStr(1)) ;
			p2ds_id = dbh.getInt(2);
			p2dd_id = dbh.getInt(3);
			TV.setText (thisdam_name);
			Log.i("format record", " Dam is " + thisdam_name);
			Log.i("format record", " Dam is " + String.valueOf(thisdam_id));
		}

		if (p2ds_id != 0){
			cmd = String.format( "select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p2ds_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec( cmd);
			Log.i("format record", " after second db lookup");
			cursor2   = ( Cursor ) crsr2;
			cursor2.moveToFirst();
			p2ds_name = ("          " + dbh.getStr(0)+ " " + dbh.getStr(1));
			p3dss_id = dbh.getInt(2);
			p3dsd_id = dbh.getInt(3);
			TV = (TextView) findViewById( R.id.p2dsName );
			TV.setText (p2ds_name);
			Log.i("format record", " Sire is " + p2ds_name);
			Log.i("format record", " Sire is " + String.valueOf(p2ds_id));
		}

		if (p2dd_id != 0){
			cmd = String.format( "select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p2dd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec( cmd);
			Log.i("format record", " after third db lookup");
			cursor2   = ( Cursor ) crsr2;
			cursor2.moveToFirst();
			p2dd_name = ("          " + dbh.getStr(0)+ " " + dbh.getStr(1));
			p3dds_id = dbh.getInt(2);
			p3ddd_id = dbh.getInt(3);
			TV = (TextView) findViewById( R.id.p2ddName );
			TV.setText (p2dd_name);
			Log.i("format record", " Dam is " + p2dd_name);
			Log.i("format record", " Dam is " + String.valueOf(p2dd_id));
		}

		if (p3dss_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p3dss_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after fourth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p3dss_name = ("               " + dbh.getStr(0) + " " + dbh.getStr(1));
			p4dsss_id = dbh.getInt(2);
			p4dssd_id = dbh.getInt(3);
			TV = (TextView) findViewById(R.id.p3dssName);
			TV.setText(p3dss_name);
			Log.i("format record", " Sire is " + p3dss_name);
			Log.i("format record", " Sire is " + String.valueOf(p3dss_id));
		}

		if (p3dsd_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p3dsd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after fifth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p3dsd_name = ("               " + dbh.getStr(0) + " " + dbh.getStr(1));
			p4dsds_id = dbh.getInt(2);
			p4dsdd_id = dbh.getInt(3);
			TV = (TextView) findViewById(R.id.p3dsdName);
			TV.setText(p3dsd_name);
			Log.i("format record", " Dam is " + p3dsd_name);
			Log.i("format record", " Dam is " + String.valueOf(p3dsd_id));
		}

		if (p3dds_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p3dds_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after sixth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p3dds_name = ("               " + dbh.getStr(0) + " " + dbh.getStr(1));
			p4ddss_id = dbh.getInt(2);
			p4ddsd_id = dbh.getInt(3);
			TV = (TextView) findViewById(R.id.p3ddsName);
			TV.setText(p3dds_name);
			Log.i("format record", " Sire is " + p3dds_name);
			Log.i("format record", " Sire is " + String.valueOf(p3dds_id));
		}

		if (p3ddd_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p3ddd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after seventh db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p3ddd_name = ("               " + dbh.getStr(0) + " " + dbh.getStr(1));
			p4ddds_id = dbh.getInt(2);
			p4dddd_id = dbh.getInt(3);
			TV = (TextView) findViewById(R.id.p3dddName);
			TV.setText(p3ddd_name);
			Log.i("format record", " Dam is " + p3ddd_name);
			Log.i("format record", " Dam is " + String.valueOf(p3ddd_id));
		}

		if (p4dsss_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4dsss_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after eighth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4dsss_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4dsssName);
			TV.setText(p4dsss_name);
			Log.i("format record", " Sire is " + p4dsss_name);
			Log.i("format record", " Sire is " + String.valueOf(p4dsss_id));
		}

		if (p4dssd_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4dssd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after ninth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4dssd_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4dssdName);
			TV.setText(p4dssd_name);
			Log.i("format record", " Dam is " + p4dssd_name);
			Log.i("format record", " Dam is " + String.valueOf(p4dssd_id));
		}

		if (p4dsds_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4dsds_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after tenth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4dsds_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4dsdsName);
			TV.setText(p4dsds_name);
			Log.i("format record", " Sire is " + p4dsds_name);
			Log.i("format record", " Sire is " + String.valueOf(p4dsds_id));
		}

		if (p4dsdd_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4dsdd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after eleventh db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4dsdd_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4dsddName);
			TV.setText(p4dsdd_name);
			Log.i("format record", " Dam is " + p4dsdd_name);
			Log.i("format record", " Dam is " + String.valueOf(p4dsdd_id));
		}

		if (p4ddss_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4ddss_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after twelfth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4ddss_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4ddssName);
			TV.setText(p4ddss_name);
			Log.i("format record", " Sire is " + p4ddss_name);
			Log.i("format record", " Sire is " + String.valueOf(p4ddss_id));
		}

		if (p4ddsd_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4ddsd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after thirteenth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4ddsd_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4ddsdName);
			TV.setText(p4ddsd_name);
			Log.i("format record", " Dam is " + p4ddsd_name);
			Log.i("format record", " Dam is " + String.valueOf(p4ddsd_id));
		}

		if (p4ddds_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4ddds_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after fourteenth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4ddds_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4dddsName);
			TV.setText(p4ddds_name);
			Log.i("format record", " Sire is " + p4ddds_name);
			Log.i("format record", " Sire is " + String.valueOf(p4ddds_id));
		}

		if (p4dddd_id != 0) {
			cmd = String.format("select flock_prefix_table.flock_name, sheep_table.sheep_name, sheep_table.sire_id, sheep_table.dam_id " +
					" from sheep_table left join flock_prefix_table on sheep_table.id_flockprefixid = flock_prefix_table.flock_prefixid " +
					" where sheep_table.sheep_id = '%s'", p4dddd_id);
			Log.i("format record", " cmd is " + cmd);
			crsr2 = dbh.exec(cmd);
			Log.i("format record", " after fifteenth db lookup");
			cursor2 = (Cursor) crsr2;
			cursor2.moveToFirst();
			p4dddd_name = ("                         " + dbh.getStr(0) + " " + dbh.getStr(1));
			TV = (TextView) findViewById(R.id.p4ddddName);
			TV.setText(p4dddd_name);
			Log.i("format record", " Dam is " + p4dddd_name);
			Log.i("format record", " Dam is " + String.valueOf(p4dddd_id));
		}


		Log.i("FormatRecord", " before formatting results");
		// go get tags
//		cmd = String.format( "select sheep_table.sheep_name,  id_type_table.idtype_name, " +
//				"tag_colors_table.tag_color_name, id_info_table.tag_number, id_location_table.id_location_abbrev, " +
//				"id_info_table.id_infoid as _id, id_info_table.tag_date_off  " +
//				"from sheep_table inner join id_info_table on sheep_table.sheep_id = id_info_table.sheep_id " +
//				"left outer join tag_colors_table on id_info_table.tag_color_male = tag_colors_table.id_tagcolorsid " +
//				"left outer join id_location_table on id_info_table.tag_location = id_location_table.id_locationid " +
//				"inner join id_type_table on id_info_table.tag_type = id_type_table.id_typeid " +
//				"where id_info_table.sheep_id ='%s' and (id_info_table.tag_date_off is null or " +
//				"id_info_table.tag_date_off is '')order by idtype_name asc", thissheep_id);
//
//
//		Log.i("format record", " comand is " + cmd);
//		crsr = dbh.exec( cmd );
//		Log.i("format record", " after run the command");
//		tagcursor   = ( Cursor ) crsr;
//		tagcursor.moveToFirst();
//		Log.i("format record", " the cursor is of size " + String.valueOf(dbh.getSize()));
//
//		//	Get set up to try to use the CursorAdapter to display all the tag data
//		//	Select only the columns I need for the tag display section
//		String[] fromColumns = new String[ ]{ "tag_number", "tag_color_name", "id_location_abbrev", "idtype_name"};
//		Log.i("FormatRecord", "after setting string array fromColumns");
//		//	Set the views for each column for each line. A tag takes up 1 line on the screen
//		int[] toViews = new int[] { R.id.tag_number, R.id.tag_color_name, R.id.id_location_abbrev, R.id.idtype_name};
//		Log.i("FormatRecord", "after setting string array toViews");
//		myadapter = new SimpleCursorAdapter(this, R.layout.list_entry, tagcursor ,fromColumns, toViews, 0);
//		Log.i("FormatRecord", "after setting myadapter");
//		setListAdapter(myadapter);
//		Log.i("FormatRecord", "after setting list adapter");

		// Now we need to check and see if there is an alert for this sheep
//   	Log.i("Alert Text is " , alert_text);
//	Now to test of the sheep has an alert and if so then display the alert & set the alerts button to red
		if (alert_text != null && !alert_text.isEmpty() && !alert_text.trim().isEmpty()){
			// make the alert button red
			Button btn = (Button) findViewById( R.id.alert_btn );
			btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFFCC0000));
			btn.setEnabled(true);
			//	testing whether I can put up an alert box here without issues
			showAlert(v);
		}

	}

	//  user clicked 'Scan' button
	public void scanEid( View v){
		// Here is where I need to get a tag scanned and put the data into the variable LastEID
		clearBtn( v );
		//	needs to read from tag id array and get the location of the one that corresponds to electronic
		// forced it correct here but needs work
		// TODO: 2/25/19
		tag_type_spinner.setSelection(1);
		if (mService != null) {
			try {
				//Start eidService sending tags
				Message msg = Message.obtain(null, eidService.MSG_SEND_ME_TAGS);
				msg.replyTo = mMessenger;
				mService.send(msg);
				//	make the scan eid button  green 0x0000FF00, 0xff00ff00
				Button btn = (Button) findViewById( R.id.scan_eid_btn );
				btn.getBackground().setColorFilter(new LightingColorFilter(0x0000FF00, 0xff00ff00));

			} catch (RemoteException e) {
				// In this case the service has crashed before we could even do anything with it
			}
		}
	}
	public boolean tableExists (String table){
		try {
			dbh.exec("select * from "+ table);
			return true;
		} catch (SQLiteException e) {
			return false;
		}
	}

	public void helpBtn( View v )
	{
		// Display help here
		AlertDialog.Builder builder = new AlertDialog.Builder( this );
		builder.setMessage( R.string.help_look_up_sheep)
				.setTitle( R.string.help_warning );
		builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int idx) {
				// User clicked OK button
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	// user clicked the 'back' button
	public void backBtn( View v )
	{
//    	Log.i("Back Button", " In the lookupsheep back code at beginning");
		doUnbindService();
//    	Log.i("Back Button", " In lookupsheep back after dounbindservice");
		stopService(new Intent(TestInterfaceDesigns.this, eidService.class));
//    	Log.i("Back Button", " In lookupsheep back after stop service");
		// Added this to close the database if we go back to the main activity
		//	Close cursors if there are any but fall out if we don't have any in use
		try {
//			Log.i("Back Button", " In try stmt cursor");
			cursor.close();
		}
		catch (Exception e) {
//			Log.i("Back Button", " In catch stmt cursor");
			// In this case there is no adapter so do nothing
		}
		try {
//			Log.i("Back Button", " In try stmt cursor2");
//			stopManagingCursor (cursor2);
			cursor2.close();
		}
		catch (Exception e) {
//			Log.i("Back Button", " In catch stmt cursor2");
			// In this case there is no adapter so do nothing
		}
		try {
//			Log.i("Back Button", " In try stmt cursor3");
//			stopManagingCursor (cursor3);
			cursor3.close();
		}
		catch (Exception e) {
//			Log.i("Back Button", " In catch stmt cursor3");
			// In this case there is no adapter so do nothing
		}
		dbh.closeDB();
		clearBtn( null );
		//Go back to main
		finish();
	}

	public void showAlert (View v){
//    		String	alert_text;
		String          cmd;
		Object 			crsr2;

		// Display alerts here
		AlertDialog.Builder builder = new AlertDialog.Builder( this );
		Log.i("ShowAlert", "Alert Text is" + alert_text);
		builder.setMessage( alert_text )
				.setTitle( R.string.alert_warning );
		builder.setPositiveButton( R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int idx) {
				// User clicked OK button
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
//    				cursor2.close();
	}

	// user clicked 'clear' button
	public void clearBtn( View v )
	{
//	    Log.i("clear btn", "got here");
		thissheep_id = 0;
		// TextView TV ;

		// Log.i("clear btn", "before clearing input");
		TextView TV = (TextView) findViewById( R.id.inputText );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.sheepnameText );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.sireName );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.damName );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.birth_weight );
		TV.setText( "" );
		//   Log.i("clear btn", "after clearing sire to birth weight");

		TV = (TextView) findViewById( R.id.birth_date );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.birth_type );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.sheep_sex );
		TV.setText( "" );
		// Log.i("clear btn", "after clearing birth date to sex");

		TV = (TextView) findViewById( R.id.owner );
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.death_reason);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.transfer_date);
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.death_date );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.cluster_name );
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.codon);
		TV.setText( "" );

		//	Log.i("clear btn", "after cluster to codon");

		//	Need to clear out the rest of the tags here
		TV = (TextView) findViewById( R.id.p2ssName );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.p2sdName );
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p3sssName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p3ssdName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p3sdsName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p3sddName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4ssssName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4sssdName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4ssdsName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4ssddName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4sdssName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4sdsdName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4sddsName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4sdddName);
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.p2dsName );
		TV.setText( "" );
		TV = (TextView) findViewById( R.id.p2ddName );
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p3dssName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p3dsdName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p3ddsName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p3dddName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4dsssName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4dssdName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4dsdsName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4dsddName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4ddssName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4ddsdName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4dddsName);
		TV.setText( "" );
		TV = (TextView) findViewById(R.id.p4ddddName);
		TV.setText( "" );

		//	Log.i("clear btn", "before changing myadapter");
		try {
			myadapter.changeCursor(null);
		}
		catch (Exception e) {
			// In this case there is no adapter so do nothing
		}
		try {
			//		Log.i("lookup clrbtn", " before set notes to null");
			myadapter2.changeCursor(null);
		} catch (Exception e) {
			// In this case there is no adapter so do nothing
		}
		//	Log.i("clear btn", "after changing myadapter and myadapter2");

		try {
			drugAdapter.changeCursor(null);
		} catch (Exception e) {
			// In this case there is no adapter so do nothing
		}
	}
	public void doNote( View v )
	{
		Utilities.takeNote(v, thissheep_id, this);
	}

	// user clicked the "next record" button
	//
	public void nextRecord( View v)
	{
		//	Clear out the display first
		clearBtn( v );
		//	Go get the sheep id of this record
		Log.i("in next record", "this sheep ID is " + String.valueOf(thissheep_id));
		cursor.moveToNext();
		Log.i("in next record", "after moving the cursor ");
		thissheep_id = cursor.getInt(0);
		Log.i("in next record", "this sheep ID is " + String.valueOf(thissheep_id));
		recNo         += 1;
		formatSheepRecord(v);
//    		// I've moved forward so I need to enable the previous record button
		Button btn3 = (Button) findViewById( R.id.prev_rec_btn );
		btn3.setEnabled(true);
		if (recNo == (nRecs)) {
			// at end so disable next record button
			Button btn2 = (Button) findViewById( R.id.next_rec_btn );
			btn2.setEnabled(false);
		}
	}

	// user clicked the "previous record" button
	public void prevRecord( View v){
//	    	Clear out the display first
		clearBtn( v );
		Log.i("in prev record", "this sheep ID is " + String.valueOf(thissheep_id));
		cursor.moveToPrevious();
		Log.i("in prev record", "after moving the cursor ");
		thissheep_id = cursor.getInt(0);
		Log.i("in prev record", "this sheep ID is " + String.valueOf(thissheep_id));
		recNo         -= 1;
		formatSheepRecord(v);
		// I've moved back so enable the next record button
		Button btn2 = (Button) findViewById( R.id.next_rec_btn );
		btn2.setEnabled(true);
		if (recNo == 1) {
			// at beginning so disable prev record button
			Button btn3 = (Button) findViewById( R.id.prev_rec_btn );
			btn3.setEnabled(false);
		}
	}
}
