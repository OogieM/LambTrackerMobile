package com.weyr_associates.lambtracker;

import android.app.Activity;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class AdvancedToolsMenu extends Activity {

	public Button btn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.advanced_tools_menu);
		// make the sort sheep button normal and disabled
//		btn = (Button) findViewById( R.id.sort_sheep_btn );
//		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
//		btn.setEnabled(false);
//		// make the group sheep management button normal and disabled
//		btn = (Button) findViewById( R.id.group_sheep_management_btn );
//		btn.getBackground().setColorFilter(new LightingColorFilter(0xFF000000, 0xFF000000));
//		btn.setEnabled(false);
	}
	public void editDB (View v){
		Intent i = null;
		i = new Intent(AdvancedToolsMenu.this, EditDB.class);
		AdvancedToolsMenu.this.startActivity(i);
	}
	public void desktopActions (View v){
		Intent i = null;
		i = new Intent(AdvancedToolsMenu.this, DesktopFunctionsMenu.class);
		AdvancedToolsMenu.this.startActivity(i);
	}
}
